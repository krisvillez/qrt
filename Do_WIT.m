function WIT    =   Do_WIT(QR,Links)
%
%   Do_WIT
%   -----------------------------------------------------------------------
%
%   Construct Wavelet Interval Tree on the basis of qualitative
%   representations and established links between aligned essential points
%
%   I/O: WIT    =   Do_WIT(QR,Links)
%
%   inputs:
%       QR:         Cell array of qualitative representations. Each cell
%                   has a qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       Links:      Cell array of links between aligned points in
%                   neighbouring scales. Each cell has the pairs of aligned
%                   points as a matrix with each row representing one
%                   alignment. The first column contains the indices of the 
%                   points in the first sequence. The second column
%                   contains the indicates in the indices of the aligned 
%                   points in the second sequence. This input is obtained
%                   via the Links function.
%
%   outputs:
%       WIT:        Wavelet Interval Tree (WIT) as structure;
%       WIT.Monotonic.QR
%                   Matrix of triangular episodes (each as row) with
%                   identification of which episode they are branched to.
%       WIT.Monotonic.Times
%                   Start and end times of triangular episodes at the
%                   different scales they appear. Needed for plotting only.
%       WIT.Triangular.QR
%                   Matrix of triangular episodes (each as row) with
%                   identification of which episode they are branched to.
%       WIT.Triangular.Times
%                   Start and end times of triangular episodes at the
%                   different scales they appear. Needed for plotting only.
%
%   Originally by:      Kris Villez, 08/12/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

visual  =   0           ;   % set visual to 1 for visuals of intermediate results (for debugging)

if visual
    PTS     =   QR2PTS(QR)  ;   % convert QR structure to point sequence
    figure
    PlotQRT(QR)
    PlotLinks(PTS,Links)
    PlotPTS(PTS)
end

% 1. Create Tree
Tri                     =   Tree(QR,Links)      ;

% 2. "Collapse" tree into monotonic episodes
[Mon,Tri]               =   Monotonic(Tri)      ;

% 3. Merge episodes at different scales IN MONOTONIC WIT
[Mon,MonTimes,Tri]      =   Merge(Mon,Tri)      ;
 
% 4. Merge episodes at different scales IN TRIANGULAR WIT
[Tri,TriTimes]          =   SubMerge(Tri,Mon)   ;
    
Mon(:,1:2)              =   Mon(:,11:12)        ;   % replace start/end times with corresponding times at bottom scale
Tri(:,1:2)              =   Tri(:,11:12)        ;   % replace start/end times with corresponding times at bottom scale     
WIT.Monotonic.QR        =   Mon                 ;   % store Monotonic WIT
WIT.Triangular.QR       =   Tri                 ;   % store Triangular WIT

WIT.Monotonic.Time      =   MonTimes            ;   % store time information for MWIT
WIT.Triangular.Time     =   TriTimes            ;   % store time information for TWIT

if visual
    figure
        hold on
        PlotWIT(WIT,1,0)
        PlotLineage(WIT,1)
    figure
        hold on
        PlotWIT(WIT,0,0)
        PlotLineage(WIT,0)
end



% =========================================================================
function WIT    =   Tree(QR,Links)

% This function creates tree structure

% numbers
nScale                  =   length(QR)          ;   % maximal scale
n1                      =   zeros(nScale,1)     ;   % initialize: numbers of features at each scale 
n                       =   0                   ;   % initialize: total number of features
for i=1:nScale                                      % run over all scales
    n1(i)               =   size(QR{i},1)       ;   %   count number of features in representation at this scale
    n                   =   n+n1(i)             ;   %   add up to count
end

% setup matrix
WIT                     =   zeros(n,10)         ;   % initialize WIT as matrix
n                       =   0                   ;   % set counter to zero

for i=1:nScale  % add all features into one matrix
    QRi =   QR{i}       ;
    QRi =   QRi(:,1:4)  ;
    WIT(n+(1:n1(i)),:)  =   [   flipud(QRi)   ones(n1(i),2)*i     zeros(n1(i),4)  ]   ;   
    % - flipud is used to arrange features from left to right and from coarse scale to finer scales
    % - fifth last column is scale
    % - last 4 columns are initialized as zero (no links yet)
    n                   =   n+n1(i)             ;   % counter update
end

WIT                     =   flipud(WIT)         ;   % now features appear in order: from left to right and from coarse scale to finer scales
WIT(:,7)                =   (1:n)'              ;   % add unique index to each feature
WIT(:,9:10)             =   WIT(:,1:2)          ;   % repeat start and end index
WIT(:,11:12)            =   WIT(:,1:2)          ;   % repeat start and end index

% find links and add parental links to matrix
for i=1:nScale-1                                    % run over all scales (until second last one)
    Loc1                =   find(WIT(:,5)==i)   ;   %   find features at current scale
    Loc2                =   find(WIT(:,5)==i+1) ;   %   find features at higher scale
    
    Link                =   Links{i}            ;   %   get links between essential points in the two scales
    nLink               =   length(Link)        ;   %   number of links
    
%     disp([' Scale: ' num2str(i)])   ;
%     Link
%     Loc1
%     Loc2
    
    for iL=1:nLink-1                                                % for all the links (until second last, last = at end of time series)
        Parent          =   Loc2(Link(iL,2))                    ;   %   find the parent feature
        Children        =   Loc1(Link(iL,1):Link(iL+1,1)-1)     ;   %   find the children features: all features in finer scale until the next one that is linked
        WIT(Children,8) =   WIT(Parent,7)                       ;   %   store link: all children get index of parent in 8th column  
        WIT(Parent,9)   =   min(WIT(Children,1))                ;   %   obtain minimum time of immediate children
        WIT(Parent,10)  =   max(WIT(Children,2))                ;   %   obtain maximum time of immediate children
        WIT(Parent,11)  =   min(WIT(Children,11))               ;   %   obtain minimum time borrowed from children at lowest scale
        WIT(Parent,12)  =   max(WIT(Children,12))               ;   %   obtain maximum time borrowed from children at lowest scale
    end
    
end

% =========================================================================
function [Tree2,Tree1]   =   Monotonic(Tree1)

% Obtain Monotonic WIT from Triangular WIT

n               =       size(Tree1,1)           ;   % number of Triangular features
nQR             =       max(Tree1(:,5))         ;   % maximal index of features
Tree2           =       Tree1                   ;   % initialize: just copy the TWIT for MWIT, 
Tree1           =   [   Tree1   zeros(n,2)  ]   ;   % add a column to TWIT to link with monotonic feature

for i=nQR:-1:1                                      %   run over features in reverse order (coarse scale to finer scale)
    Loc         =       find(Tree2(:,5)==i)     ;   %       find all features with this index
    [oo,ii]     =       sort(Tree2(Loc,1))      ;   %       sort according to start time
    Loc         =       Loc(ii)                 ;   %       sort according to start time
    Keep        =   [   1;  find(diff(Tree2(Loc,3))~=0)+1;  length(Loc)+1   ];  % keep those features where sign of first derivative has changed
    nKeep       =       length(Keep)-1          ;   %       number of kept features

    for j=1:nKeep                                   %   run over all kept features
        Replace                     =   Keep(j)+1:Keep(j+1)-1       ;   % find features that are to be remove following merging into one monotonic feature
        Join                        =   Keep(j):Keep(j+1)-1         ;   % find all features that are to be merged into one monotonic feature
        LocJoin                     =   Loc(Join)                   ;   % find index in original matrix
        if ~isempty(Replace)                                            % if some features do in fact disappear
            LocReplace              =   Loc(Replace)                ;           % get their index
            Children                =   GetLoc(Tree2(:,8),Tree2(LocJoin,7)) ;   % get all features in the TWIT covered merged into the resulting monotonic episode
            Tree2(LocJoin(1),2)     =   Tree2(LocReplace(end),2)    ;       % update end time of monotonic feature (with end time of last triangular feature to be merged) at this scale
            Tree2(LocJoin(1),10)    =   Tree2(LocReplace(end),10)   ;       % update end time of monotonic feature (with end time of last triangular feature to be merged) at immediate children's scale
            Tree2(LocJoin(1),12)    =   Tree2(LocReplace(end),12)   ;       % update end time of monotonic feature (with end time of last triangular feature to be merged) at bottom scale
            Tree1(LocJoin,13)       =   Tree2(LocJoin(1),7)         ;       % store link between triangular features and monotonic feature
            Tree2(Children,8)       =   Tree2(LocJoin(1),7)         ;       % update link in the monotonic tree
            Tree2(LocReplace,:)     =   0                           ;       % set all elements for replaced features to zero
        else                                                            % if no merging is necessary
            Tree1(LocJoin(1),13)    =   Tree2(LocJoin(1),7)         ;       % store link between the two trees
        end
    end
end

include         =   find(Tree2(:,7)~=0)         ;   % find features for which index has not been set to zero
Tree2           =   Tree2(include,:)            ;   % remove all other features
Tree2(:,4)      =   0                           ;   % set second order derivative to zero for all features (since this info is removed by merging)

% =========================================================================
function  [Tree,Times,Tree1] =    Merge(Tree,Tree1,nScale)

% this function merges equal features existing at several scales into
% single features spanning multiple scales

n                   =   size(Tree,1)                    ;   % number of features
DoTree1             =   (nargin>=2 && ~isempty(Tree1))  ;   % true if second tree is provided
if nargin<3 || isempty(nScale)                              % true if maximal scale is not provided
    nScale          =   max(Tree(:,6))                  ;       % maximal scale up to which merging is necessary
end
mScale              =   max(Tree(:,6))                  ;   % maximal scale

Time1               =   zeros(n,nScale+1)               ;   % initialize
Time2               =   zeros(n,nScale+1)               ;   % initialize

Top                 =   find(Tree(:,6)==mScale)         ;   % find features at most coarse scale
Time1(Top,mScale+1) =   Tree(Top,1)'                    ;   %  store the start time at this scale
Time2(Top,mScale+1) =   Tree(Top,2)'                    ;   %  store the end time at this scale
Tree(Top,8)         =   0                               ;   % set links for top features to 0
        
Active              =   ones(n,1)                       ;   % set all features to active

while any(Active)                                           % as long as certaint features have not been checked
    Parent          =   find(Active,1,'first')          ;   %   find first active parent in list
    Children        =   find(Tree(:,8)==Tree(Parent,7)) ;   %   find index of children of this parent
    TreeChild       =   Tree(Children,:)                ;   %   get children
    nC              =   length(Children)                ;   %   number of children
    switch nC                                               %   according to the number of children
         case 0                                             %       no children
            S1                      =   Tree(Parent,5)  ;   %           minimal scale at which feature exists
            Time1(Parent,S1)        =   Tree(Parent,9)  ;   %           add start time instant
            Time2(Parent,S1)        =   Tree(Parent,10) ;   %           add end time instant
            Active(Parent)          =   0               ;   %           deactivate parent
            
       case 1                                              %       1 child: must be the same -> merge into one feature
            TreeJoin                =   [ Tree(Parent,:) ; TreeChild ]  ;   % features to be joined
            S1                      =   min(TreeJoin(:,5))              ;   % minimal scale of merged features
            S2                      =   max(TreeJoin(:,6))              ;   % maximal scale of merged features
            Tree(Parent,3:4)        =   Tree(Children,3:4)              ;   % sign of derivates: inherited from child
            Tree(Parent,5:6)        =   [ S1 S2 ]                       ;   % store scales of merged features
            Tree(Parent,9)          =   min(TreeChild(:,9))             ;   % minimal start time of merged features
            Tree(Parent,10)         =   max(TreeChild(:,10))            ;   % maximal end time of merged features
            Tree(Children,:)        =   0                               ;   % mark all children for removal by setting all elements to zero
            if DoTree1                                                      % if another tree was provided (Tree1)
                Loc                 =   GetLoc(Tree1(:,13),TreeJoin(:,7));  %   find features in Tree1 that are linked to merged features
                Tree1(Loc,13)       =   Tree(Parent,7)                  ;   %   update corresponding links in Tree1
            end

            Time1(Parent,[S2 S1]+1) =   TreeJoin(:,1)'                  ;   % store start times at each scale the merged feature exists
            Time2(Parent,[S2 S1]+1) =   TreeJoin(:,2)'                  ;   % store end times at each scale the merged feature exists

            GrandChildren           =   GetLoc(Tree(:,8),TreeChild(:,7));   % find children of children (grandchildren)
            Tree(GrandChildren,8)   =   Tree(Parent,7)                  ;   % update links so to link to new merged feature

            Active(Children)        =   0                               ;   % deactive parent feature

        otherwise                                              % more than 1 child
            S1                      =   max(TreeChild(:,5))             ;   % maximum of miminum scales at which children exist
            Time1(Parent,S1+1)      =   min(TreeChild(:,1))             ;   % add minimal start time for parent
            Time2(Parent,S1+1)      =   max(TreeChild(:,2))             ;   % add maximal end time for parent
            Time1(Children,S1+1)    =   TreeChild(:,1)                  ;   % add minimal start time for children
            Time2(Children,S1+1)    =   TreeChild(:,2)                  ;   % add maximal end time for children
            Tree(Parent,9)          =   min(TreeChild(:,9))             ;   % update start time of immediate children
            Tree(Parent,10)         =   max(TreeChild(:,10))            ;   % update end time of immediate children

            Active(Parent)          =   0                               ;   % deactivate parent

    end
    
end

Times               =   [ Time1 Time2 ]     ;   % Join time information

include             =   find(Tree(:,1)~=0)  ;   % remove features that are marked for deletion
Tree                =   Tree(include,:)     ;   % remove features that are marked for deletion
Times               =   Times(include,:)    ;   % remove features that are marked for deletion


% =========================================================================
function  [Tri,Times] =    SubMerge(Tri,Mon)

nTri                        =   size(Tri,1)             ;   % number of features in monotonic tree
n                           =   size(Mon,1)             ;   % number of features in monotonic tree
nScale                      =   max(Mon(:,6))           ;   % maximal number of scales
Times                       =   zeros(nTri,(nScale+1)*2)   ;   % initialize: time information

for i=1:n                                                   % for all monotonic features
    id                      =   Mon(i,7)                ;   %   get index
    Loc                     =   find(Tri(:,13)==id)     ;   %   find triangular episodes linked to this monotonic episode
    nLoc                    =   length(Loc)             ;   %   number of triangular episodes
    
%    assert(nLoc>0,'Problem')    % if there are no triangular episode -> something seriously wrong
    
    SubTri                  =   Tri(Loc,:)              ;   % find subtree of features linked to monotonic tree
    [SubTri,SubTimes]       =   Merge(SubTri,[],nScale) ;   % merge the triangular features across scales  
    nLoc2                   =   size(SubTri,1)          ;   % number or merged features
    vec                     =   Loc(1:nLoc2)            ;   % find new location for merged features
    Tri(vec,:)              =   SubTri                  ;   % store merged triangular features
    Tri(Loc(nLoc2+1:end),:) =   0                       ;   % mark remainder for deletion
    Times(vec,:)            =   SubTimes                ;   % store corresponding times
    
end

include                     =   find(Tri(:,1)~=0)       ;   % remove features that are marked for deletion
Tri                         =   Tri(include,:)          ;   % remove features that are marked for deletion
Times                       =   Times(include,:)        ;   % remove features that are marked for deletion


% =========================================================================

function Loc = GetLoc(A,B)

%na  =   length(A)   ;
%nb  =   length(B)   ;
%Loc =   find(any(ones(nb,1)*A(:)'==B(:)*ones(1,na)))    ;

% Old version: consumes a lot of time
%Loc = find(ismember(A,B,'rows'))   ;

Loc = find(ismember(A,B))   ;	% works in octave and Matlab
%Loc = find(ismembc(A,B))   ;	% only for Matlab

% =========================================================================



