function [QR,Links,PTS]  =   Do_Linking(QR,verbose,visual,Ts)
%
%   Do_Linking
%   -----------------------------------------------------------------------
%
%   Establish alignments of essential points across each pair of
%   neighbouring wavelet scales based
%
%   I/O: [QR,Links,PTS]  =   Do_Linking(QR,visual,verbose,Ts)
%
%   inputs:
%       QR:         Cell array of qualitative representations. Each cell
%                   has a qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       visual:     [optional] indicate whether visual output should be
%                   delivered. 0: off / 1: on
%                   [default=0]
%       visual:     [optional] indicate whether command line output should
%                   be delivered. 0: off / 1: on
%                   [default=0]
%       Ts:         [optional] sample time
%                   [default=1]
%
%   outputs:
%       QR:         Cell array of qualitative representations, where
%                   spurious essential points are removed. Each cell has a
%                   qualitative representation as a matrix with each row
%                   representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       Links:      Cell array of links between aligned points in
%                   neighbouring scales. Each cell has the pairs of aligned
%                   points as a matrix with each row representing one
%                   alignment. The first column contains the indices of the
%                   points in the first sequence. The second column
%                   contains the indicates in the indices of the aligned
%                   points in the second sequence.
%       PTS:        Cell array of point type sequences. Each cell contain
%                   the point type sequence for the corresponding
%                   qualitative representation in the input as a matrix.
%                   The first column is the time index of the point and the
%                   second column is the qualitative indicator for the
%                   point. See function QR2PTS.m for more details.
%
%   Originally by:      Kris Villez, 11/12/2009
%   Last update by:     Kris Villez, 11/01/2011
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ---------------------------------------------------
% default settings: set to 1 for debugging (visual / verbose)
verbose_default =   0   ;
visual_default  =   0   ;
% ---------------------------------------------------

if nargin<2 || isempty(visual)
    visual  =   visual_default ;
end
if nargin<3 || isempty(verbose)
    verbose =   verbose_default ;
end

% ----------------------------
% 1. transform each Qualitative Representation (QR) into a Point Type
% Sequence (PTS)
PTS     =   QR2PTS(QR)      ;

if visual
    figure(99)
    PlotQRT(QR,0,Ts)
    PlotPTS(PTS,Ts)
%     figure(199)
%     PlotQRT(QR,0,Ts)
%     PlotPTS(PTS,Ts)
end

% ----------------------------
% 2. link points in PTS across scales
nPTS    =   length(PTS)     ;
Links   =   cell(nPTS-1,1)  ;

for i= 1:nPTS-1   % run over all scales until second last

%     t1  =   cputime ;
    if verbose, disp(['                                 Do_Linking: Level - ' num2str(i) ]), end

    L1              =   i                       ;   % lower level
    L2              =   i+1                     ;   % higher level

    PTS1            =   PTS{L1}                 ;   % point sequence at lower level
    PTS2            =   PTS{L2}                 ;   % point sequence at higher level
    n2              =   length(PTS2)            ;   % length of sequence 2

    Links{i}        =   Do_Align(PTS1,PTS2) ;
    nL              =   size(Links{i},1) ;

%     if visual
%         figure(99)
%         PlotLinks(PTS,Links,Ts) ;
%     end
    
    if nL~=n2                                       
        % if not all points at the higher level are matched, then remove the spurious points
        % AdjustQR add links for spurious points at the sides, these are
        % then removed via the RemoveDeadEnds function. AdjustQR also
        % contains code for removal of spurious point internally.

        QR1         =   QR{i+1}                 ;   % get qualitative representation at higher scale
        L1          =   Links{i}                ;   % get links at current level
        [QR1,L1]    =   AdjustQR(QR1,L1)        ;   % performance adjustment
        QR{i+1}     =   QR1                     ;   % save adjusted qualitative representation
        Links{i}    =   L1                      ;   % save adjusted links
        PTS         =   QR2PTS(QR)              ;
    end
    
%     if visual
%         figure(199)
%         PlotLinks(PTS,Links,Ts) ;
%     end
%     cputime-t1
%     pause
end

% if visual
%     PlotAlign(QR,Links) ;
% end

if nPTS>1
    [QR,Links ] =   RemoveDeadEnds(QR,Links);
end
if nargout>=3
    PTS     =   QR2PTS(QR)                  ;   % convert the adjusted set of qualitative representations to adjusted point sequences
end

if visual
    PlotAlign(QR,Links) ;
end


return

% =========================================================================
function [QR,Links] =   AdjustQR(QR,Links)

nP          =   size(QR,1)                  ;   % number of episodes
Remove      =   setdiff(1:nP+1,Links(:,2))  ;   % remove those episodes that are not linked
Remove      =   sort(Remove)                ;   % sort those to be removed
nRem        =   length(Remove)              ;   % number of episodes to be removed

% Spurious points at the left are handled later
[FirstOne,FirstLoc] =   ismember(2,Remove)              ;
if FirstOne
    Ref             =   (1:nRem)-1+Remove(FirstLoc)     ;   % index with respect to first point to be removed
    Contig          =   Remove==Ref                     ;   % location of neighbouring spurious points at the left in vector Remove
    ContigL         =   Remove(Contig)                  ;   % actual point index of neighbouring spurious points at the left
    Remove          =   setdiff(Remove,ContigL)         ;   % spurious points at the left are discarded
else
    ContigL         =   []                              ;
end

nRem                =   length(Remove)              ;   % number of episodes to be removed
[LastOne,LastLoc]   =   ismember(nP,Remove)             ;  
if LastOne
    Ref             =   (1:nRem)-nRem+Remove(LastLoc)   ;   % index with respect to last point to be removed
    Contig          =   Remove==Ref                     ;   % location of neighbouring spurious points at the right in vector Remove
    ContigR         =   Remove(Contig)                  ;   % actual point index of neighbouring spurious points at the right
    Remove          =   setdiff(Remove,ContigR)         ;   % spurious points at the right are discarded
else
    ContigR         =   []                              ;
end

% figure(99)
% plot((QR(Remove,1)-1)*5/60,ones(length(Remove),1)*5,'cd')
% Spurious points at the right are handled later
% assert(isempty(Remove),'Internal spurious points - check!')
% I suppose spurious points only appear at the edges. If they should
% appear, code below solves the problem

% Remove spurious points in the middle
while ~isempty(Remove)                          % as long as some episodes to be removed remain    
    Rem             =   Remove(end)             ;   % take the last one to be removed
    %   if this is neither second or second last episode
    inc             =   setdiff(1:nP,Rem)       ;   %       index of those to keep
    QR(Rem-1,2)     =   QR(Rem,2)               ;   %       set end time of episode just before the one to be removed to the end time of the one to be removed
    QR              =   QR(inc,:)               ;   %       remove selected episode
    Loc             =   Links(:,2)>Rem          ;   %       find index of links to be updated (i.e. after the one removed)
    Links(Loc,2)    =   Links(Loc,2)-1          ;   %       update selected links
    nP              =   size(QR,1)              ;   %       update number of episodes
    Remove          =   setdiff(Remove,Rem)     ;   %       update set of episodes to be removed
end

% Links are added: Spurious points at the left and right (in coarse scale) are linked to the
% utter left and right position (in detail scale). These creates double
% links from the detail scale. This is then resolved via the RemoveDeadEnds
% function

Links   =   [   Links; [ones(length(ContigL),1) ContigL(:)];  [ones(length(ContigR),1)*Links(end,1) ContigR(:)] ];
Links   =       sortrows(Links)     ;

% =========================================================================
function [QR,Links ] = RemoveDeadEnds(QR,Links)

visual      =   0               ;   % set to 1 for debugging

nL          =   size(Links,1)   ;   % number of links
Edge        =   zeros(nL,2)     ;

Link        =   Links{1}        ;
CurrentL    =   1               ;
CurrentR    =   Link(end,1)     ;

for iL=1:nL
    Link        =   Links{iL}                   ;
    nLink       =   size(Link,1)                ;
    First       =   find(Link(:,1)==CurrentL,1,'last')      ;
    Last        =   find(Link(:,1)==CurrentR,1,'first')     ;
    if isempty(First)
        First   =   1       ;
    end
    if isempty(Last)
        Last    =   nLink   ;
    end
    IndexL      =   First:Last-1                ;
    IndexR      =   First+1:Last                ;
    
    if isempty(IndexL)
        CurrentL    =   Link(1,2)                   ;
    else
        %CanL        =   find(Link(IndexL,1)==Link(IndexL(1),1)) ;
        CanL        =   Link(IndexL,1)==Link(IndexL(1),1)       ;
        CurrentL    =   Link(max(IndexL(CanL)),2)               ;
    end
    if isempty(IndexR)
        CurrentR    =   Link(end,2)                 ;
    else
        %CanR        =   find(Link(IndexR,1)==Link(IndexR(end),1));
        CanR        =   Link(IndexR,1)==Link(IndexR(end),1)     ;
        CurrentR    =   Link(min(IndexR(CanR)),2)               ;
    end
    Edge(iL,1)      =   CurrentL    ;
    Edge(iL,2)      =   CurrentR    ;
end

if visual
    EdgeLinks           =   cell(nL,1)  ;
    for iL=1:nL
        Link            =   Links{iL}   ;
        EdgeLinks{iL}   =   [   Link(Edge(iL,1),1)  Link(Edge(iL,1),2) ; Link(Edge(iL,2),1)  Link(Edge(iL,2),2)  ];
    end
    PlotAlign(QR,PTS,EdgeLinks) ;
end

for iL=1:nL
    
    e1          =   Edge(iL,1)          ;
    e2          =   Edge(iL,2)          ;

    qr          =   QR{iL+1}            ;
    np          =   size(qr,1)+1        ;
    if e2<np     % Remove parts right of edge
        qr(e2-1,2)  =   qr(end,2)       ;
        qr          =   qr(1:e2-1,:)    ;

        if iL<nL
            j           =   1                       ;
            link        =   Links{iL+1}             ;
            link        =   RemLinkRight(link,j,e2) ;
            Links{iL+1} =   link                    ;
        end

        j           =   2                           ;
        link        =   Links{iL}                   ;
        link        =   RemLinkRight(link,j,e2)     ;
        Links{iL}   =   link                        ;

    end

    if e1>1    % Remove parts left of edge
        qr(e1,1)    =   qr(1,1)         ;
        qr          =   qr(e1:end,:)    ;

        if iL<nL
            j           =   1                       ;
            link        =   Links{iL+1}             ;
            link        =   RemLinkLeft(link,j,e1)  ;
            Links{iL+1} =   link                    ;
        end

        j           =   2                       ;
        link        =   Links{iL}               ;
        link        =   RemLinkLeft(link,j,e1)  ;
        Links{iL}   =   link                    ;
    end

    QR{iL+1}    =   qr              ;
    
end

if visual
    PlotAlign(QR,Links) ;
end

% ===================================================================
function link = RemLinkLeft(link,j,e1)

loc             =   find(link(:,j)>=e1+1,1,'first')     ;
link            =   [ link(1,:); link(loc:end,:)   ]    ;
link(2:end,j)   =   link(2:end,j)-e1+1                  ;


% ===================================================================
function link = RemLinkRight(link,j,e2)

loc             =   find(link(:,j)<=e2-1,1,'last')      ;
link            =   [ link(1:loc,:); link(end,:)   ]    ;
link(end,j)     =   e2                                  ;
