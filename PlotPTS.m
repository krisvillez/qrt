function PlotPTS(PTS,Ts)
%
%   PlotPTS
%   -----------------------------------------------------------------------
%
%   Vizualize point sequence with different markers according to type
%
%   I/O: PlotPTS(PTS,Ts)
%
%   inputs:
%       PTS:        Cell array of point type sequences. Each cell contain
%                   the point type sequence for the corresponding
%                   qualitative representation in the input as a matrix.
%                   The first column is the time index of the point and the
%                   second column is the qualitative indicator for the
%                   point. See function QR2PTS.m for more details.
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 29/11/2010
%   Last update by:     Kris Villez, 30/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<2 || isempty(Ts)
    Ts = 1;
end
hold on
if ~isa(PTS,'cell')
    PTS     =   { PTS }         ;
end
n           =   length(PTS)     ;

for i=1:n
    
    PTSmat      =   PTS{i}                      ;
    nPTSmat     =   size(PTSmat,1)              ;

    X           =   PTSmat(:,1)                 ;   %   X coordinates
    X           =   (X-1)*Ts                    ;
    Y           =   ones(nPTSmat,1)*i           ;   %   Y coordinates
    C           =   PTSmat(:,2)+5               ;   %   point type index
    UniC        =   unique(C)                   ;
    nUniC       =   length(UniC)                ;
    symbol      =   {'yx','wx','yx','wo','md','ws','y+','w+','y+'}  ;
    for i=1:nUniC
        CI      =   UniC(i)         ;
        Loc     =   find(C==CI)     ;
        str     =   symbol{CI}      ;
        str     =   'w.'             ;
        plot(X(Loc),Y(Loc),str,'MarkerSize',7)
    end
end
   
drawnow;
axis tight


