function PTSs    =   QR2PTS(QRs,explicit)
%
%   QR2PTS
%   -----------------------------------------------------------------------
%
%   Convert qualitative representation (QR) into point type sequence (PTS)
%
%   I/O: PTS =   QR2PTS(QR,explicit)
%
%   inputs:
%       QR:         Cell array of qualitative representations. Each cell
%                   has a qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       explicit:   [optional] indicate whether inflection points of the
%                   same type (accelerating/decelerating) should be
%                   considered different if the sign of the first
%                   derivative is different (1) or not (0).
%                   [default=0]            
%   outputs:
%       PTS:        Cell array of point type sequences. Each cell contain
%                   the point type sequence for the corresponding
%                   qualitative representation in the input as a matrix.
%                   The first column is the time index of the point and the
%                   second column is the qualitative indicator for the
%                   point.
%
%   When explicit is set to 0 (false, default), the qualitative indicators are as
%   follows:
%       +3: accelerating inflection (minimal speed)
%       +2: right-most point
%       +1: minimum
%       -1: maximum
%       -2: left-most point        
%       -3: decelerating inflection (maximal speed)
%   When explicit is set to 1 (true), the qualitative indicators are as
%   follows:
%       +4: right-most point
%       +3: accelerating inflection with positive speed (minimal speed)
%       +2: minimum
%       +1: decelerating inflection with positive speed (maximal speed)
%       -1: accelerating inflection with negative speed (minimal speed)
%       -2: maximum
%       -3: decelerating inflection with negative speed (maximal speed)
%       -4: left-most point        
%   
%   Originally by:      Kris Villez, 12/08/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% explicit defines whether essential points can be linked according to one
% sign only or not. explicit = 0  means that an inflection point at B-C
% intersection is the same as A-D (accelerating), same for C-B and D-A
% inflection points. This is used to align point sequences as part of
% WIT construction. explicit = 1 means these inflection points are
% considered different. This is done for postprocessing, e.g. to compare
% two sequences which are supposed to be the same in view of performance
% assessment.

if nargin<2 || isempty(explicit)
    explicit = 0 ;
end

nQR         =   length(QRs) ;   % number of scales
PTSs        =   cell(nQR,1) ;   % initialize: as many cell elements as scales
for iQR=1:nQR                   % for each scale
    
    QR      =   QRs{iQR}    ;   %   get episodes
    nEpi    =   size(QR,1)  ;   %   number of episodes
    
    v1      =   1:nEpi-1    ;   %   index of episodes except the last one
    v2      =   2:nEpi      ;   %   index of episodes except the first one
    if explicit
        % extremum types
        Type1   =   (QR(v1,3)~=QR(v2,3)).*(2*QR(v2,3))    ;
        % (true for extremum point) .* (sign after point)
        % 0 for inflection point
        % -2 for maximum
        % +2 for minimum
        % inflection types
        Type2   =   (QR(v1,3)==QR(v2,3)).*(2*QR(v2,3)+QR(v2,4))  ;
        % (true for inflection point) .* (sign after point)
        % 0 for extremum
        % -3,1 for decelerating inflection (maximal speed)
        % +3,-1 for accelerating inflection (minimal speed)
        TypeL       =   -4  ;   
        TypeR       =   4   ;
        % types used for left-most and right-most point
    else
        % extremum types
        Type1   =   (QR(v1,3)~=QR(v2,3)).*(QR(v2,3))    ;
        % (true for extremum point) .* (sign after point)
        % 0 for inflection point
        % -1 for maximum
        % +1 for minimum
        % inflection types
        Type2   =   3*(QR(v1,3)==QR(v2,3)).*(QR(v2,4))  ;
        % (true for inflection point) .* (sign after point)
        % 0 for extremum
        % -3 for decelerating inflection (maximal speed)
        % +3 for accelerating inflection (minimal speed)
        TypeL       =   -2  ;
        TypeR       =   2   ;
        % types used for left-most and right-most point
    end
    % combined type
    Type    =   Type1+Type2                         ;
                % -1 for maximum
                % +1 for minimum
                % -3 for decelerating inflection (maximal speed)
                % +3 for accelerating inflection (minimal speed)

        
    Time        =   [   QR(:,1);    QR(end,2)           ];  % store time information
    Type        =   [   TypeL;      Type;       TypeR   ];  % store type information
    PTS         =   [   Time        Type                ];  % combine and store
    
    PTSs{iQR}   =       PTS   ;   % store for this scale
    
end