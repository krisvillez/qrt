function PlotLineage(WIT,Monotonic,Ts)
%
%   PlotLineage
%   -----------------------------------------------------------------------
%
%   Vizualize branches in Wavelet Interval Tree (WIT)
%
%   I/O: PlotLineage(WIT,Monotonic,Ts)
%
%   inputs:
%       WIT:        Wavelet Interval Tree
%       Monotonic:  Decide to use Nonotonic (1) or Triangular (0) WIT
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 09/12/2009
%   Last update by:     Kris Villez, 30/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


if nargin<2 || isempty(Monotonic)
    Monotonic = 0;
end
if nargin<3 || isempty(Ts)
    Ts          =   1   ;
end

%assert(any((0:1)==Monotonic),'Not viable option')
if Monotonic
    WIT     =   WIT.Monotonic       ;
else
    WIT     =   WIT.Triangular      ;
end


WIT         =   WIT.QR              ;
    
n   =   size(WIT,1)      ;

XX  =   zeros(n,2)  ;
YY  =   zeros(n,2)  ;
C   =   0           ;

for i=1:n
    
    Children    =   find(WIT(:,8)==WIT(i,7)) ;
    
    if ~isempty(Children)
        X2          =   sum(WIT(i,1:2),2)/2         ;
        Y2          =   ones(length(X2),1)*WIT(i,6) ;
        X1          =   sum(WIT(Children,1:2),2)/2  ;
        nC          =   length(X1)                  ;
        Y1          =   ones(nC,1)*(WIT(i,5)-1)     ;
        X2          =   ones(nC,1)*X2               ;
        Y2          =   ones(nC,1)*Y2               ;
        X           =   [X1 X2]                     ;
        Y           =   [Y1 Y2]                     ;

        XX(C+(1:nC),:)  =   X       ;
        YY(C+(1:nC),:)  =   Y       ;
        C               =   C+nC    ;
    end
end

XX      =   (XX-1)*Ts   ;

plot(XX',YY','w.-')
