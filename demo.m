% demo
%   This script executes a demo of QRT for a given time series.

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

% -------------------------
% GET DATA
load data
Ts  =   5/60        ;   % sampling time (min.)
nY  =   length(Y)   ;
tt  =   1:nY        ;

% -------------------------
% GET METHOD OPTIONS
options.method  =   {'Original','Top','Bottom','Witkin'}  ; % methods to be used
options.visual  =   0   ;   % level of vizualization during execution
options.verbose =   0 	;   % level of verbosity during execution
options.demo    =   0   ;   % demo execution (with pause) or not (no pause)

% -------------------------
% EXECUTE
QR      =   QRT(Y,options,Ts)   ;
[m,n]   =   size(QR)            ;
for j=1:m
    STR{j,1} = [ options.method{j} ' method: ' ];
    for k=1:n
        STR{j,k+1} = [  QR2string(QR(j,k).Triangular) ]   ;
    end
end

% page_output_immediately(0)

% -------------------------
% DISPLAY
figure
FigPrep
subplot(3,1,1)
    AxisPrep
    plot(tt*Ts,Y,'k.-')
    axis tight
    set(gca,'Xticklabel',[])
    ylabel('Data')
subplot(3,1,2)
    AxisPrep
    PlotQRT({QR(:).Monotonic},1,Ts)
    xlabel('Time [min.]')
    set(gca,'Yticklabel',options.method)
    ylabel('Method')
subplot(3,1,3)
    AxisPrep
    PlotQRT({QR(:).Triangular},2,Ts)
    xlabel('Time [min.]')
    set(gca,'Yticklabel',options.method)
    ylabel('Method')

disp('')
disp('Triangular representations as strings:')

disp(STR)

