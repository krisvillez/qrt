QRT toolbox v2.0 - Qualitative Representation of Trends
by Kris Villez

Website:	http://web.ics.purdue.edu/~kvillez/
Contact: 	kris.villez@gmail.com

This toolbox is a collection of Matlab/Octave functions and scripts I have written for execution of the original Qualitative Representation of Trends method and its extensions. They are released to the public under the GPL v3 license in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the file LICENSE.TXT located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R14, R2009b, R2010b, R2011a) and Octave (v3.2.4) on a Windows system. To install the Matlab functions in a Windows 98/NT/2000/XP/Vista system, using Matlab or Octave, follow the instructions below.

---------------------------
 I. Installing the toolbox
---------------------------

1. Create a directory to contain the Matlab functions.  For example, in my case the directory is

    C:\Tools\QRT

2. Put the file "QRT.zip" into this directory.

3. Extract the function files using a program such as WinZip or 7-Zip (available on the web). This directory contains all necessary functions as well as a demo script and demo data.

-----------------------
 II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the QRT toolbox from any other folder, add the toolbox to the Matlab/Octave path by typing the following in the command line

	addpath C:\Tools\QRT

This allows access during the current Matlab/Octave session.

3. To enable access to the QRT toolbox permanently (across sessions), type the following in the command line:
	
	savepath

4. To test and demo the QRT toolbox, execute the script 'demo.m' located in the QRT toolbox (e.g. C:\Tools\QRT). At the command line, type:
	
	demo

5. For help with the QRT toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:
	
	help QRT

----------------------------
 III. Relevant publications
----------------------------

[1] Bakshi, B. R. & Stephanopoulos, G. (1994). Representation of process trends - part III. Multiscale extraction of trends from process data. Computers and Chemical Engineering, 18, 267�302.
[2] Villez, K., Rosen, C., Anctil, F., Duschesne, C. & Vanrolleghem, P. A. (2011). Qualitative representation of trends: an improved method for multiscale extraction of trends from process data. Accepted to Computers and Chemical Engineering, In Press. - available upon request.

-------------
 IV. Changes
-------------

v1.1:	
- Do_Filter_Trending.m: modified for faster filtering in Octave
- QRT.m: 		modified for immediate output of disp command in Octave
- PlotHorizontal: 	plotting suppressed in Octave as a quick-fix to problem with axis command
- PlotVertical: 	plotting suppressed in Octave as a quick-fix to problem with axis command

v1.2:	
- QRT.m: 		The trivial case of a constant signal was not centered properly resulting in faulty analysis. This is modified so that after centering the signal has zero-value.

v1.3:	
- Do_Align_DynProgFW.m: The Case where no link can be found ('dead end' in solution path) was left unaccounted. This is now solved by breaking the loop and returning an empty solution path. When this happens, this eventually leads to allowing to skip features in the coarser representation in Do_Align_DynProg.m

v2.0:
- This version as used for the paper [2]. 
- One file name changed: 'Plot_Filter_Trending.m' is now 'Do_Filter_Trending_DisplayScript.m'
- Some cosmetic changes in 'demo.m'


------------------
 Acknowledgements
------------------

I want to thank the following people for testing the toolbox and their subsequent suggestions:
- Gernaey, Krist
- Torabi, Korosh

----------------------------------------------------------
Last modified on 29 August 2012

