function PlotBoxes(CanLink,Links,Lines)
%
%   PlotBoxes
%   -----------------------------------------------------------------------
%
%   Vizualize candidate links, obtained links and boxes (used for debugging of AlignPTS)  
%
%   I/O: PlotBoxes(CanLink,Links,Lines)
%
%   inputs:
%       CanLink:    Matrix with zeros where links are impossible, ones
%                   where possible (blue colour). 
%       Links:      Matrix of proposed selection of links (red colour)
%       Lines:      Lines to plot (to indicate splits).
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 29/11/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

disp('PlotBoxes busy...')
[n1,n2] =   size(CanLink) ;

figure
hold on
axis([0 n2 0 n1])   ;

[y,x] =   find(CanLink)     ;
PlotPatch(x,y,'b')

if nargin>=2 && ~isempty(Links)
    PlotPatch(Links(:,2),Links(:,1),'r')
end

if nargin>=3 && ~isempty(Lines)
%     vertical_line(Lines(:,1)-[1; 0],'r-','linewidth',2)
%     horizontal_line(Lines(:,2)-[1; 0],'r-','linewidth',2)
    vertical_line(Lines(:,2),'r-','linewidth',2)
    horizontal_line(Lines(:,1),'r-','linewidth',2)
end
set(gca,'Ydir','reverse')

disp('PlotBoxes done...')
drawnow
pause

function PlotPatch(x,y,str)

X   =  [ x-1    x   x   x-1 ]   ;
Y   =  [ y-1    y-1 y   y   ]   ;
patch(X',Y',str)    ;

