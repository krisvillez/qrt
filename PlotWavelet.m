function han = PlotWavelet(T,Y,A,D,TitleString)
%
%   PlotWavelet
%   -----------------------------------------------------------------------
%
%   Vizualize wavelet decomposition
%
%   I/O: PlotWavelet(T,Y,A,D)
%
%   inputs:
%       T:          Time vector
%       Y:          Original time series
%       A:          Approximation signals
%       D:          Detail signals
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 08/10/2010
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<5 || isempty(TitleString)
    TitleString =   ' '     ;
end
    
[kmax,maxscale] =   size(A) ;

Amin    =   min(A)  ;
Amax    =   max(A)  ;
Dmin    =   min(D)  ;
Dmax    =   max(D)  ;

A       =   2* (A-ones(kmax,1)*(Amax+Amin)/2)./(1.2*ones(kmax,1)*(Amax-Amin));
D       =   (D)./(1.2*ones(kmax,1)*(Dmax-Dmin));

Octave	=	VerifyOctave	;

ncol    =   2           ;
nrow    =   maxscale+1  ;

han     =   figure    ;
FigPrep
subhan  =   subplot(nrow,ncol,1)   ;

shiftL  =   0.06    ;
shiftR  =   0.04    ;

LineWidth   =   1.2 ;
AxisPrep
plot(T,Y,'k-','LineWidth',LineWidth)
axis tight
axes        =   axis            ;
axes(3:4)   =   axes(3:4)*1.1   ;
axis(axes)

set(gca,'Xticklabel',[])
set(gca,'Yticklabel',[])
set(gca,'Ytick',0)
if Octave
    ylabel('Y','rotation',0,'VerticalAlignment','middle')
else
    Xlim    =   get(gca,'Xlim')     ;
    Xrange  =   Xlim(2)-Xlim(1)     ;
    ylabel('Y','rotation',0,'VerticalAlignment','middle','Position',[Xlim(1)-Xrange*15/200 0])
end
shift   =   shiftL    ;
if ~Octave
    subroutine(subhan,shift)
end

for col = 1:2
    if col == 1
        data    =   A       ;
        str1    =   'a'     ;
        shift   =   shiftL  ;
    elseif col==2
        data    =   D       ;
        str1    =   'd'     ;
        shift   =   shiftR  ;
    end
    for row =2:nrow
        subhan = subplot(nrow,ncol,(row-1)*2+col)   ;
        
        AxisPrep
        
        plot(T,data(:,row-1),'k-','LineWidth',LineWidth)
        axis tight
        if row~=nrow
            set(gca,'Xticklabel',[])
        end
        set(gca,'Ytick',0)
        set(gca,'Ylim',[-1 1])
        
		if ~Octave
			subroutine(subhan,shift)
        end
        
        if col==2
            set(gca,'Yticklabel',0)
            horizontal_line(0,'k:','LineWidth',LineWidth)
			if Octave
                ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle')
            else
                Xlim    =   get(gca,'Xlim')     ;
                Xrange  =   Xlim(2)-Xlim(1)     ;
				ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle','Position',[Xlim(2)+Xrange*15/200 0])
            end
            if row == 2
                title(TitleString)  
            end
        else
            set(gca,'Yticklabel',[])
			if Octave
				ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle')
			else
                Xlim    =   get(gca,'Xlim')     ;
                Xrange  =   Xlim(2)-Xlim(1)     ;
				ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle','Position',[Xlim(1)-Xrange*15/200 0])
			end
        end
    end
    xlabel('Time [min.]')
end

drawnow

function subroutine(subhan,shift)

Posi =  get(subhan,'Position')              ;
Posi =  [Posi(1:2)+[-shift 0.02] 0.4 0.065] ;
set(subhan,'Position',Posi) ;
