function PlotVertical(level,plotstr,varargin)

% Plots vertical line (suppressed in Octave).

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

level           =   level(:)'   ;
nlevel          =   length(level);
ax              =   axis    ;
if VerifyOctave

else
    if nargin<2 || isempty(plotstr)
        plotstr     =   'k-'    ;
    end
    plot(ones(2,1)*level,(ax(3:4))'*ones(1,nlevel),plotstr,varargin{:})
end