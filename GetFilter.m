function [h,g,hc,gc]    =   GetFilter(filter,order,visual)
%
%   GetFilter
%   -----------------------------------------------------------------------
%
%   Get a pair of low-pass and high-pass filters
%
%   I/O: [h,g,hc,gc]    =   GetFilter(filter,order,visual)
%
%   inputs:
%       filter:     Name of filter bank, available:
%                       'Mallat1989' [1]
%                       'Mallat1992' [2]
%                       'Bakshi1994' [3]
%                       'Bakshi1994mod' (modification of Bakshi1994)
%       order:      [optional] order of filter. Only relevant for
%                   Mallat1992. [default=2]
%       visual:     [optional] specify need for graphical output.
%                   [default: 0]
%
%   outputs:
%       h:          low-pass filter
%       g:          high-pass filter
%       hc:         location of maximum in low-pass filter
%       gc:         location of zero-crossing in high-pass filter
%
%   Literature:
%   [1] Mallat, S. G. (1989). A theory for multiresolution signal
%   decomposition: The wavelet representation. IEEE Trans. on Pattern
%   Analysis and Machine Intelligence, 11:674�693.  
%   [2] Mallat, S.; Zhong, S. (1992). Characterization of Signals from
%   Multiscale Edges. IEEE TRans. on Pattern Analysis and Machine
%   Intelligence, 14(7), 710 
%   [3] Bakshi, B. R.; Stephanopoulos, G. (1994). Representation of process
%   trends - Part III. multiscale extraction of trends from process data.
%   Comput. Chem. Eng., 18, 267-302.
%
%   Originally by:      Kris Villez, 18/08/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<2 || isempty(order),  order   =   2   ;   end     % use default order if not specified
if nargin<3 || isempty(visual), visual  =   0   ;   end     % use default setting if not specified

filter  =   upper(filter)  ;    % set to upper case

if strcmp(filter,'MALLAT1989')
    delta   =       .01                                 ;   % frequency resolution
    w       =   [   delta:delta:1 -1:delta:-delta ]*pi  ;   % frequencies
    
    n       =       4   ;
    SIG     =       SIGn(w,n)        ; 
    SIG2    =       SIGn(2*w,n)      ;
    H       =       (SIG ./ ( 2^(2*n) .* SIG2 )).^(1/2)     ;  
    h       =       ifft(H,[],'symmetric')     ;
    kmax    =       floor(size(w,2)/2) ;
    kk      =       -kmax:kmax                        ;
    
    g   =   zeros(2*kmax,1) ;
    ng  =   g               ;
    for ii = 1:2*kmax
        ni          =   kk(ii)              ;   % = 1-n
        ng(ii)      =   (1-ni)              ;   % = n
        g(ii)       =   (-1).^(ni)*h(ii)    ;
    end
    [oo,si]     =   sort(ng)    ;
    g           =   g(si)       ;
    g           =   g(1:2*kmax)   ;
    G           =   fft(g)      ;
    N=12;
    [w,ii]  =   sort(w)                 ;
    H   =   H(ii)   ;   G   =   G(ii)   ; 
    if visual
        FIRplots(N,{h,g})
        SPECplots(w,{H,G})
    end
    hc  =   1   ;
    gc  =   1   ;
    
elseif strcmp(filter,'MALLAT1992')
    
    delta   =       .01                                 ;   % frequency resolution
    w       =   [   delta:delta:1 -1:delta:-delta ]*pi  ;   % frequencies
    
    p   =       order*2-1                           ;
    n   =       (p-1)/2                             ;
    H   =       exp(i*w/2).*(cos(w/2)).^(2*n+1)     ;   
    G   =       4*i*exp(i*w/2).*(sin(w/2))          ;  
    K   =       (1-(abs(H)).^2)./G        ;
    w   =   [   0   w       ]   ;
    H   =   [   1       H   ]   ;   
    G   =   [   0   G       ]   ; 
    K   =   [   0   K       ]   ; 
    h   =       ifft(H,[],'symmetric')  ; 
    g   =       ifft(G,[],'symmetric')  ;
    k   =       ifft(K,[],'symmetric')  ;
    [w,ii]  =   sort(w)                 ;
    H   =   H(ii)   ;   G   =   G(ii)   ;   K   =   K(ii)   ;
    N   =   12  ;
    if visual
        FIRplots(N,{h,g,k})
        SPECplots(w,{H,G,K})
    end
    hc  =   1   ;
    gc  =   1   ;
elseif strcmp(filter,'BAKSHI1994')
    h   =   [   .0625       .2500       .3750       .2500       .0625   ]   ;
    g   =   -[   -.00008     -.01643     -.10872     -.59261     .59261   .10872   .01643  .00008   ]   ;
    h   =       h(:)        ;
    g   =       g(:)        ;
    hc  =       3           ;
    gc  =       4.5         ;
%     if visual
%         FIRplots(n,{h,g,k})
%     end
elseif strcmp(filter,'BAKSHI1994MOD')
    % lowpass filter the same as in original method. high-pass filter
    % changed so to have anti-symmetry. Helps in locating zero-crossings
    % better.
    h   =   [   .0625   .2500   .3750   .2500   .0625   ]   ;
    g   =   [   32/7    40/27   0       -40/27  -32/7   ]   ;
    h   =       h(:)    ;
    g   =       g(:)    ;
    hc  =       3       ;
    gc  =       3       ;
end


function FIRplots(n,f)

figure
hold on
cc = {'b','r','k'} ;
for j=1:length(f)
    f1 = f{j}   ;
    stem(1:n,f1(1:n),'o','Color',cc{j},'MarkerFaceColor',cc{j})
end


function SPECplots(w,f)

w = w./pi ;
figure
cc = {'b','r','k'}  ;
nj  =   length(f)   ;
for j=1:nj
    f1  =   f{j}    ;
    subplot(nj,1,j)
    hold on
    size(w)
    size(f1)
    plot(w,real(f1),'-','Color',cc{j},'MarkerFaceColor',cc{j})
    plot(w,imag(f1),'--','Color',cc{j},'MarkerFaceColor',cc{j})
%     plot(w,abs(f1),'-','Color',cc{j},'MarkerFaceColor',cc{j},'LineWidth',2)
end

function SIG = SIGn(w,n)

if n==4
    N1  =   5 + 30*(cos(w/2).^2)+   30*(sin(w/2).^2).*(cos(w/2).^2)  ;
    N2  =   2* (sin(w/2).^4).*(cos(w/2).^2)+   70*(cos(w/2).^4) + 2/3 * (sin(w/2)).^6    ;
    SIG = (N1+N2)./(sin(w/2).^(8)) ;
else
end