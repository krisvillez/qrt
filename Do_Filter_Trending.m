function [QRs,AA]    =   Do_Filter_Trending(Y,Ts,mirror,maxscale,bank,visual)   
%
%   Do_Filter
%   -----------------------------------------------------------------------
%
%   Perform filtering via wavelet filter bank
%
%   I/O: [D,A]  = Do_Filter_Trending(Y,Ts,mirror,maxscale,bank,visual) 
%
%   inputs:
%       Y:          Data. Individual time series as columns.
%       Ts:         [optional] Sampling time
%                   [default] 1
%       mirror:     [optional] Two-element vector. Defines type of padding.
%                   See function Mirror for detailed information.
%                   [default] [ 0 0]
%       maxscale:   [optional] index of last dyadic scale to be included in
%                   the analysis. (higher = rougher scale, lower = more
%                   detailed scale). 
%                   [default] log2(n) with n = length of series.
%       bank:       [optional] filter bank to be used [default: 'Villez']
%       visual:     [optional] specify need for graphical output.
%                   [default: 0]
%
% NOTES:
%   (!) current filters cannot be used for reconstruction
%
%       1. Low-pass filter used as in original method
%       2. High-pass filter changed for better inflection isolation. This
%       filter is not 'scaled'. Pair of filters is not a QMF so
%       reconstruction is not possible.
%
%   outputs:
%       QRs:        Qualitative representations
%
%   Originally by:      Kris Villez, 15/08/2008
%   Last update by:     Kris Villez, 29/05/2010
%
%   -----------------------------------------------------------------------

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

%   -----------------------------------------------------------------------
%   settings for debugging
verbose     =   0   ;
visual      =   0   ;
%   -----------------------------------------------------------------------

if nargin<2 || isempty(Ts)  % if sampling time not provided, set to one
    Ts = 1;
end
if nargin<3 || isempty(mirror)  % if mirror not provided, use default (constant value padding)
    mirror  =   [ 0 0];
end
if nargin<4 || isempty(maxscale)  % if maxscale not provided, use maximal scale available
    maxscale=   log(nk)/log(2)  ;
end
if nargin<5 || isempty(bank)
    bank    =   'Bakshi1994mod'        ;
end
if nargin<6 || isempty(visual)
    visual  =   0               ;
end

Octave	=	VerifyOctave	;

% filter setup
order       =   2               ;
bank        =   lower(bank)     ;
[L,H,Lc,Hc] =   GetFilter(bank,order,visual) ;   %   L=low-pass, H=high-pass
delayH      =   length(H)-ceil(Hc)          ;   %   delay associated with high-pass filter (ank scale)
delayL      =   length(L)-Lc                ;   %   delay associated with low-pass filter on first scale
pad         =   delayL*sum(2.^(1:maxscale)) ;   %   padding: sum of delays throughout filterbank

% 1. signal padding
[Ye,k1,k2]      =       Mirror(Y,1,mirror(1),mirror(2)) ;   %   provide mirror data: 1: expand data / 1: point-wise symmetry at left / 1: point-wise symmetry at right
nk              =       size(Ye,1)                      ;   %   length time series
Ye              =       Ye - ones(nk,1)*Ye(1,:)         ;   %   substract first element
Ye              =   [   Ye; ones(pad,1)*Ye(end,:) ]     ;
T               =       ((k1:k2)-k1)*Ts                 ;

% figure
% plot(Ye)
% pause
% 2. signal filtering and trending

% initialisation
A0              =       Ye                      ;
delays          =       zeros(maxscale,1)       ;
QRs             =       cell(maxscale,1)        ;   % initialize qualitative representations

% -------------------------------------------------------------------------
if visual
    ncol        =   2           ;
    nrow        =   maxscale+1  ;
    shiftL      =   0.06        ;
    shiftR      =   0.04        ;
    LineWidth   =   1.2         ;

    han99=figure(99)
    FigPrep
    
    subhan  =   subplot(nrow,ncol,1)   ;
    AxisPrep
    plot(T,A0(k1:k2),'k-','LineWidth',LineWidth)
    axis tight
    set(gca,'Ylim',[-1 1])
    
    set(gca,'Xticklabel',[])
    set(gca,'Yticklabel',[])
    set(gca,'Ytick',0)
    if Octave
        ylabel('Y','rotation',0,'VerticalAlignment','middle')
    else
        Xlim    =   get(gca,'Xlim')     ;
        Xrange  =   Xlim(2)-Xlim(1)     ;
        ylabel('Y','rotation',0,'VerticalAlignment','middle','Position',[Xlim(1)-Xrange*15/200 0])
    end
    shift   =   shiftL    ;
    if ~Octave
        subroutine(subhan,shift)
    end
end
% -------------------------------------------------------------------------

if nargout>=2
    AA = zeros(length(k1:k2),maxscale) ;
end

% go through filter bank
for scale = 1:maxscale
    
    if verbose,  disp(['     Filtering: Scale - ' num2str(scale) ]) , end
    %   do filtering
    A1                  =   conv2(A0,L)                             ;   %   low pass filter
    D1                  =   conv2(A0,H)                             ;   %   high pass filter
    
    % truncate details
    delay               =   length(L)-Lc-1                          ;   %   delay associated with low pass filter at current scale
    delays(scale+1)     =   delay                                   ;
    D1                  =   D1(delayH+delays(scale)*2+(1:nk),:)     ;
    D1                  =   D1(k1:k2)                               ;
    
    % To the next level:
    A0                  =   A1(1:end-delay,:)   ;
    L                   =   Spacer(L)           ;
    Lc                  =   Lc*2                ;
    
    % ---------------------------------------------------------------------
    % display
    if visual
        Do_Filter_Trending_DisplayScript
      
    end
    % ---------------------------------------------------------------------
    
    if verbose,  disp(['     Trending: Scale - ' num2str(scale) ]) , end
    QR          =    Do_Trending(D1,Ts)     ;
    QRs{scale}  =   QR                      ;
    
    if nargout>=2
        AA(:,scale) = D1  ;
    end
end


% =========================================================================
function subroutine(subhan,shift)

Posi =  get(subhan,'Position')              ;
Posi =  [Posi(1:2)+[-shift 0.02] 0.4 0.065] ;
set(subhan,'Position',Posi) ;
% =========================================================================
function L = Spacer(L)

%   Aid function for dyadic filtering. Insert zeros into a given sequence

L       =   L(:)                ;
n       =   length(L)           ;
L2      =   [ L zeros(n,1) ]'   ;
L       =   L2(:)               ;
L       =   L(1:end-1)          ;