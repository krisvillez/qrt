function QR = QRT(data,options,Ts)

%
%   QRT
%   -----------------------------------------------------------------------
%
%   Perform Qualitative Representation of Trends (QRT)
%
%   I/O: qrt    =   QRT(data,maxscale,method,time)
%
%   inputs:
%       data:       data to be analyzed as two-dimensional matrix with
%                   time series as vectors
%       maxscale:   [optional] index of last dyadic scale to be included in
%                   the analysis. (higher = rougher scale, lower = more
%                   detailed scale).
%                   [default] log2(n) with n = length of series.
%       method:     [optional] method to be used for triangular representation
%                   Valid options:
%                   Original:   original method: remove inflection points
%                               at each wavelet scale
%                   Top:        find the most complex triangular represenation that
%                               corresponds to established monotonic
%                               representation
%                   Bottom:     find the least complex triangular represenation that
%                               corresponds to established monotonic
%                               representation
%                   Witkin:     find the complex triangular represenation
%                               by means of Witkin's criterion
%                   [default] 'Witkin'
%       time:       [optional] time vector. This should match the number of
%                   rows in data
%                   [default] time = 1:n
%
%   outputs:
%       qrt:        analysis result with fields
%           .triangular - Triangular Representation
%           .monotonic - Monotonic Representation
%
%   Originally by:      Kris Villez, 18/08/2009
%   Last update by:     Kris Villez, 29/08/2011
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


t0          =   cputime     ;

%assert(~isempty(data),'ERROR: input matrix has no data')  ;   % assert that actual data is provided

Y0          =   data                ;   % for display
Y           =   data                ;   % for analysis
[kmax,C]    =   size(Y)             ;   % get dimensions of data matrix


% normalize data (min -> 0, max -> 1)
Ymin        =   zeros(1,C)                                          ; 
Ymax        =   max(Y)                                              ;
Y           =   (Y-ones(kmax,1)*Ymin)./(ones(kmax,1)*(Ymax-Ymin))   ;   

% default options
default.maxscale    =   floor(log(kmax)/log(2)) ;
default.method      =   'Witkin'                ;
default.mirror      =   [0 0]                   ;
default.time        =   (0:kmax-1)'             ;
default.verbose     =   1                       ;   % controls graphical output:  0: off, 1: only final result, 2: all intermediates
default.visual      =   2                       ;   % controls command line output:  0: off, 1: only basic points, 2: all intermediates
default.demo        =   1                       ;   % controls pauses after plotting:  0: no pause, 1: do pause

if nargin<2 || isempty(options)
    maxscale    =   default.maxscale            ;
    method      =   default.method              ;
    mirror      =   default.mirror              ;
    time        =   default.time                ;
    verbose     =   default.verbose             ;
    visual      =   default.visual              ;
    demo        =   default.demo                ;
else
    if isfield(options,'maxscale'), maxscale    =   options.maxscale;   else    maxscale    =   default.maxscale;   end
    if isfield(options,'method'),   method      =   options.method  ;   else    method      =   default.method  ;   end
    if isfield(options,'mirror'),   mirror      =   options.mirror  ;   else    mirror      =   default.mirror  ;   end
    if isfield(options,'time'),     time        =   options.time    ;   else    time        =   default.time    ;   end
    if isfield(options,'verbose'),  verbose     =   options.verbose ;   else    verbose     =   default.verbose ;   end
    if isfield(options,'visual'),   visual      =   options.visual  ;   else    visual      =   default.visual  ;   end
    if isfield(options,'demo'),     demo        =   options.demo    ;   else    demo        =   default.demo    ;   end
end

if nargin<3 || isempty(Ts)
    Ts = 1;
end

kmaxT       =   length(time)            ;
%assert(kmaxT==kmax,'ERROR: length of time vector does not correspond with data dimensions')

if ~iscell(method)
    method  =   {method}                ;
end
nMethod     =   numel(method)           ;
Timing      =   zeros(nMethod,C,6)      ;

% Should inflection points be removed prior to alignment: 1: yes, 2: no
Red         =   (~strcmp(method,'Original'))+1  ; 
uRed        =   sort(unique(Red))               ;   % unique set of options
nRed        =   length(uRed)                    ;

% time
time        =   time*Ts         ;   
Tmin        =   time(1)         ;   % start time
Tmax        =   time(end)       ;   % end time
Trange      =   Tmax-Tmin       ;   % time range


QR          =   struct('Monotonic',cell(nMethod,C),'Triangular',cell(nMethod,C))    ;

Timing(:,:,1)   =   cputime-t0         ;   % Time until filtering

% All next steps are done for the individual time series
% Run over columns (left to right)
    
for c = 1:C

    Yc  =   Y(:,c)      ;
    
    if verbose, disp(['   SERIES - ' num2str(c) ' of ' num2str(C)]), end
    
    % Step 1 and 2A. Filtering - Trending
    if verbose, disp('       STEP 1+2A - Filtering and Trending ...'), end
    Trends0         =   Do_Filter_Trending(Yc,Ts,mirror,maxscale,[],0)  ;    
    if verbose==2, disp('                 Done'), end

    for iRed=1:nRed
        % Step 2B. Reduce inflection point if required
        if verbose, disp(['     INFLECTION POINTS - Method ' num2str(iRed) ' of ' num2str(nRed) ]), end
        
        select      =   find(Red==uRed(iRed))   ;
        Submethod   =   method(select)          ;
        nSub        =   length(Submethod)       ;
        
        if uRed(iRed)==1
            if verbose, disp('       Step 2B - Removing consecutive inflection points (original method) ...'), end
            Trends      =   Do_ReduceTrends(Trends0)   ;
        else
            if verbose, disp('       Step 2B - Keeping all inflection points (extended methods) ...'), end
            Trends      =   Trends0                 ;
        end
        
        if verbose==2, disp('                 Done'), end
        if visual>=2
            if verbose==2, disp('                 Display ...'), end
            DisplayTrending(Trends,[Ts Tmin Tmax])
            if verbose==2, disp('                 Done'), end
            if demo, pause, disp('                 Paused - Press space bar to continue'), end
        end
           
        % Step 3A. Alignment of corresponding essential points (extrema,
        % inflection points)
        
        if verbose, disp('       Step 3A - Alignment of essential points ...'), end
        clear TrendsLink Links PTS

        [TrendsLink,Links]      =   Do_Linking(Trends,[],[],Ts)   ;
        
        if verbose==2, disp('                 Done'), end
        if visual>=1
            if verbose==2, disp('                 Display ...'), end
            PlotAlign(TrendsLink,Links,Ts)  ;
            if verbose==2, disp('                 Done'), end
            if demo, pause, disp('                 Paused - Press space bar to continue'), end
        end
        
        % Step 3B. Construct WIT
        if verbose, disp('       Step 3B - Construction of Wavelet Interval Tree (WIT) ...'), end
        clear WIT
        
        WIT                 =   Do_WIT(TrendsLink,Links)    ;
        
        if verbose==2, disp('                 Done'), end
        if visual>=2
            if verbose==2, disp('                 Display ...'), end
            DisplayWIT(WIT,Ts)  ;
            if verbose==2, disp('                 Done'), end
            if demo, pause, disp('                 Paused - Press space bar to continue'), end
        end
                
        
        if verbose, disp('       Step 4  - Feature selection ...'), end
        for iSub=1:nSub
            % Step 4. make heuristic decision on the relevant features
        
            heuristic                   =   Submethod{iSub} ;
            if verbose, 
                disp(['                 HEURISTIC - Method ' num2str(iSub) ' of ' num2str(nSub) ' = ' heuristic ' ...'])
            end
            
            clear QRloc
            QRloc                       =   Do_Heuristics(WIT,heuristic)        ;
            QR(select(iSub),c)          =   QRloc                               ;
            
            if verbose==2,  disp('                             Done'), end
            if visual>=2
                if verbose==2, disp('                             Display'), end
                DisplayFinalResult(time,Y0(:,c),QRloc,[Ts Tmin Tmax Trange])
                if verbose==2, disp('                             Done'), end
                if demo, pause, end
            end

        end
            
    end

    if visual>=2 || (visual && C==1)
        if verbose, disp('     ALL METHODS - Display final representations'), end
        DisplayFinalResult(time,Y0(:,c),QR(:,c),[Ts Tmin Tmax Trange])
        if verbose==2, disp('                   Done'), end
        if demo, pause, end
    end
    
    drawnow
end

if visual && C>1
    if verbose==2, disp('   ALL SERIES - Display final representations'), end
    DisplayFinalResult(time,Y0,QR,[Ts Tmin Tmax Trange])
    if verbose==2, disp('                Done'), end
    if demo, pause, end
end
drawnow

return
%=====================


% =========================================================================
function DisplayTrending(QR,T)
        
Ts      =   T(1)    ;
Tmin    =   T(2)    ;
Tmax    =   T(3)    ;

figure
FigPrep
AxisPrep
PlotQRT(QR,0,Ts)
set(gca,'Xlim',[Tmin Tmax])
xlabel('Time [h]')
title('Qualitative representations at each wavelet scale')
drawnow


% =========================================================================
function DisplayWIT(WIT,Ts)

figure
FigPrep
AxisPrep
title('Triangular Qualitative Representations')
sub1    =   gca ;
axes(sub1);
PlotWIT(WIT,0,[],2,Ts)
PlotLineage(WIT,0,Ts)

figure
FigPrep
AxisPrep
title('Monotonic Wavelet Interval Tree')
sub1    =   gca ;
axes(sub1);
PlotWIT(WIT,[],[],0,Ts)
PlotLineage(WIT,1,Ts)
drawnow


% =========================================================================
function DisplayFinalResult(tt,Y,QR,T)

Ts      =   T(1)            ;
Tmin    =   T(2)            ;
Tmax    =   T(3)            ;
Trange  =   T(4)            ;
T       =   tt              ;

Octave  =	VerifyOctave	;	% Check whether this is run with Octave

nQR     =   length(QR)      ;
figure
FigPrep

na      =   3               ;
sub     =   zeros(na,1)     ;
for a = 1:na ;
    subplot(3,1,a)
    AxisPrep
    sub(a)  =   gca     ;
end

Ylabel_x        =   Tmin-1/20*Trange    ;
SubPos_x        =   Tmax+1/20*Trange    ;
for a = 1:na
    axes(sub(a))
    switch a
        case 1
            AxisPrep

            plot(T,Y,'.-')
            axis tight
            set(gca,'Xlim',[Tmin Tmax])

            set(gca,'XTickLabel',[])
            set(gca,'Ytick',[])
            axe         =   axis            ;
            SubPos_y    =   mean(axe(3:4))  ;
            text(SubPos_x,SubPos_y,'(a)','FontSize',14)

            if Octave
                ylabel('Y(t)')
            else
                axe         =   axis            ;
                Ylabel_y    =   mean(axe(3:4))  ;
                ylabel('Y(t)','Position',[Ylabel_x SubPos_y])
            end

        case 2
            AxisPrep
            PlotQRT({QR(:).Monotonic},1,Ts)
            
            axis tight
            set(gca,'Xlim',[Tmin Tmax])
            set(gca,'XTickLabel',[])
            set(gca,'Ytick',[])
            
            axe        =   axis             ;
            SubPos_y    =   mean(axe(3:4))  ;
            text(SubPos_x,SubPos_y,'(b)','FontSize',14)

            if Octave
                ylabel('Monotonic')
            else
                ylabel('Monotonic','Position',[Ylabel_x SubPos_y])
            end

        case 3
            AxisPrep
            PlotQRT({QR(:).Triangular},2,Ts)
            
            axis tight
            set(gca,'Xlim',[Tmin Tmax])
            set(gca,'Ytick',[])
            
            axe         =   axis            ;
            SubPos_y    =   mean(axe(3:4))  ;
            text(SubPos_x,SubPos_y,'(c)','FontSize',14)

            xlabel('Time [h]')
            if Octave
                ylabel('Triangular')
            else
                ylabel('Triangular','Position',[Ylabel_x SubPos_y])
            end
    end
    drawnow
end

drawnow
