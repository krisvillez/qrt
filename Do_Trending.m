function QR   =   Do_Trending(D,Ts)
%
%   Do_Trending
%   -----------------------------------------------------------------------
%
%   Perform filtering via wavelet filter bank
%
%   I/O: QR   =   Do_Trending(D,Ts)
%
%   inputs:
%       D:          Detail signal as column
%       Ts:         [optional] Sampling time. Does not affect results, only
%                   relevant for graphical output. [default: 1]
%
%   outputs:
%       QR:         Qualitative representations: A matrix with one row per
%                   identified triangular episode, each with 4 elements
%                   (start time, end time, sign 1st derivative, sign 2nd
%                   derivative).   
%
%   Originally by:      Kris Villez, 18/08/2009
%   Last update by:     Kris Villez, 29/05/2011
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ---------------------------------------------------
% settings for debugging
visual      =   0   ;
% ---------------------------------------------------

% if no sample time provided, define here as 1
if nargin<2 || isempty(Ts),     Ts      = 1         ;    end

QR          =   GetTrends(D,visual,Ts)    ;

% =========================================================================
%function QR = GetTrends(dd,method,visual,p,Ts) 
function QR = GetTrends(dd,visual,Ts)

% a. preliminaries --------------------------------------------------------
N               =   length(dd)              ;   % number of samples
%Loc             =   find(abs(dd)<10^-12)    ;   % locate signals smaller than 10^-12
Loc             =   abs(dd)<10^-12          ;   % locate signals smaller than 10^-12
dd(Loc)         =   0                       ;   % set them to zero
kk              =   (1:N)'                  ;   % artificial time index

if visual
    Tmin        =   kk(1)*Ts    	;   % minimal time
    Tmax        =   kk(end)*Ts  	;   % maximal time
    Trange      =   Tmax-Tmin       ;   % time range
    han42       =   figure      ;   % figure handle
    FigPrep
    subplot(3,1,1)
        AxisPrep
        hold on
        plot(kk*Ts,dd)
        set(gca,'Ytick',0)
        title('Trending ')
end

% b. reduce sequence ------------------------------------------------------
[ddR,kkR]       =   ReduceSequence(dd)  ;   % reduce signal: remove contiguous series of points with the same value

if visual
    MarkerSize      =   9   ;
    LineWidth       =   1.2 ;
    
    figure(han42)

    plot(kkR*Ts,ddR,'k-','LineWidth',LineWidth)
    plot([kkR(1) kkR(end)]*Ts,[0 0],'k--','LineWidth',LineWidth)

    Ylim    =   get(gca,'Ylim') ;
    Ylim    =   1.1*[-1 1]*(max(abs(Ylim)))    ;

    axis tight
    set(gca,'Xlim',[Tmin Tmax])
    set(gca,'Ylim',Ylim)

    ylabel('d','Position',[-15 0])
    text(Tmax+1/20*Trange,0,'(a)')
    set(gca,'Xticklabel',[])
end

% c. find extrema and zero-crossing ---------------------------------------

[Extrem,Cross]  =   AnalyzeSequence(ddR,kkR)    ;

if visual
    colour  =   0   ;   % black/white (0) or colour (1)
    
    figure(han42)
    if ~isempty(Extrem)
        %loc =   find(Extrem(:,3)==-1)   ;
        loc =   Extrem(:,3)==-1     ;
        xx  =   Extrem(loc,1)*Ts    ;
        yy  =   Extrem(loc,2)       ;
        if colour
            plot(xx,yy,'rv','MarkerFaceColor','r','MarkerSize',MarkerSize)
        else
            plot(xx,yy,'kv','MarkerSize',MarkerSize)
        end
        %loc =   find(Extrem(:,3)==1)    ;
        loc =   Extrem(:,3)==1      ;
        xx  =   Extrem(loc,1)*Ts    ;
        yy  =   Extrem(loc,2)       ;
        if colour
            plot(xx,yy,'r^','MarkerFaceColor','r','MarkerSize',MarkerSize)
        else
            plot(xx,yy,'k^','MarkerSize',MarkerSize)
        end
    end
    if ~isempty(Cross)
        %loc =   find(Cross(:,3)==-1)    ;
        loc =   Cross(:,3)==-1          ;
        xx  =   Cross(loc,1)*Ts     	;
        yy  =   Cross(loc,2)            ;
        if colour
            plot(xx,yy,'bd','MarkerFaceColor','b','MarkerSize',MarkerSize)
        else
            plot(xx,yy,'k+','MarkerSize',MarkerSize)
        end
        %loc =   find(Cross(:,3)==1)     ;
        loc =   Cross(:,3)==1           ;
        xx  =   Cross(loc,1)*Ts     	;
        yy  =   Cross(loc,2)            ;
        if colour
            plot(xx,yy,'bs','MarkerFaceColor','b','MarkerSize',MarkerSize)
        else
            plot(xx,yy,'kx','MarkerSize',MarkerSize)
        end
    end        
    drawnow
end

% d. integrate into qualitative representation ----------------------------

QR              =   IntegrateAnalysis(Extrem,Cross,N,dd)    ;

if visual
    
    figure(han42)
    subplot(3,1,2)
        AxisPrep
        PlotQRT(QR,2,Ts)
        
        ylabel('QR_e','Position',[-15 1])
        text(Tmax+1/20*Trange,1,'(b)')
        
        set(gca,'Ytick',[])
        set(gca,'Xticklabel',[])
        
        axis tight
        set(gca,'Xlim',[Tmin Tmax])
        
        drawnow
end

% % e. remove inflection points as in original method if neccessary ---------
% if strcmp(upper(method),'ORIGINAL')
%     QR          =   OneInflectionOnly(QR)  ;
% end

%QR              =   QR(:,1:4)   ;   % remove unnecessary columns

if visual
    
    figure(han42)
    subplot(3,1,3)
        axisprep
        PlotQRT(QR,2,Ts)
        
        xlabel('Time [h]')
        ylabel('QR_o','Position',[-15 1])
        text(Tmax+1/20*Trange,1,'(c)')
        
        set(gca,'Ytick',[])
        
        axis tight
        set(gca,'Xlim',[Tmin Tmax])
    
        drawnow
%     printname   =   ['.\Example\Ex3_trending_scale_' num2str(p) ] ;
%     print(FigHan, '-deps2',printname)
    
end


% =========================================================================
function [REDdd,REDkk] = ReduceSequence(dd)

N                   =   length(dd)                  ;   % length of series
kk                  =   1:N                         ;   % time index
REDkk               =   zeros(N,1)                  ;   % initialize reduced time index
REDdd               =   zeros(N,1)                  ;   % initialize reduced series
C                   =   1                           ;   % current index in reduced series
D                   =   0                           ;   % current number of accumulated equal values
REDkk(C)            =   kk(C)                       ;   % assign current index
REDdd(C)            =   dd(C)                       ;   % assign current value

for i=2:N                                               % run over series
    if dd(i)==REDdd(C)                                  %   if current value is the same as the new one
        D           =   D+1                         ;   %       go one further in original series
        REDkk(C)    =   (REDkk(C)*D + kk(i))/(D+1)  ;   %       update index for reduced series
    else                                                %   if current value is the different from the new one
        D           =   0                           ;   %       reset to zero
        C           =   C+1                         ;   %       go one further in reduced series
        REDkk(C)    =   kk(i)                       ;   %       store new point in reduced series (index)
        REDdd(C)    =   dd(i)                       ;   %       store new point in reduced series (value)
    end
end

REDkk               =   REDkk(1:C)                  ;   % remove unused space
REDdd               =   REDdd(1:C)                  ;   % remove unused space
REDkk(1)            =   1                           ;   % force first index to 1
REDkk(end)          =   N                           ;   % force last index to N


% =========================================================================
function [Extrem,Cross] = AnalyzeSequence(dd,kk)

% find extrema and zero-crossings in sequence

N           =   length(dd)  ;   % length of series

% 1. extrema --------------------------------------------------------------
TypExtrem   =   zeros(N,1)  ;   % initialize: extremum type
LocExtrem   =   zeros(N,1)  ;   % initialize: extremum location
ValExtrem   =   zeros(N,1)  ;   % initialize: extremum value
C           =   0           ;   % initialize: extremum count

for i=2:N-1 % go from second to second last sample
    signL   =   sign(dd(i)-dd(i-1)) ;   % difference to the left
    signR   =   sign(dd(i)-dd(i+1)) ;   % difference to the right
    if and(signL==-1,signR==-1)         
        % both negative: minimum
        C               =   C+1     ;   %   counter + 1
        TypExtrem(C)    =   -1      ;   %   extremum type: -1 (min)
        LocExtrem(C)    =   kk(i)   ;   %   extremum location
        ValExtrem(C)    =   dd(i)   ;   %   extremum value
    elseif and(signL==1,signR==1)       
        % both positive: maximum
        C               =   C+1     ;   %   counter + 1
        TypExtrem(C)    =   +1      ;   %   extremum type: +1 (max)
        LocExtrem(C)    =   kk(i)   ;   %   extremum location
        ValExtrem(C)    =   dd(i)   ;   %   extremum value
    end
end

Extrem      =   [ LocExtrem ValExtrem TypExtrem ];   % bundle all info
Extrem      =   Extrem(1:C,:)                    ;   % remove unused space

% 2. zero-crossings -------------------------------------------------------
TypCross    =   zeros(N,1)  ;   % initialize: zero-crossing type
LocCross    =   zeros(N,1)  ;   % initialize: zero-crossing location
ValCross    =   zeros(N,1)  ;   % initialize: zero-crossing value
C           =   0           ;   % initialize: zero-crossing count

for i=2:N
    signC   =   sign(dd(i))     ;   % sign of current value
    if signC==0 && i~=N             
        % if current value = 0 and current point is not the last point
        signL           =   sign(dd(i-1))       ;   % sign of value at the left
        signR           =   sign(dd(i+1))       ;   % sign of value at the right
        if and(signL==-1,signR==+1) 
            % if sign at the left = neg and sign at the right = pos:
            % zero-crossing, pos type (accelerating) 
            C           =   C+1                 ;   % counter + 1
            TypCross(C) =   +1                  ;   % zero-crossing type: +1
            LocCross(C) =   kk(i)               ;   % zero-crossing location
            ValCross(C) =   dd(i)               ;   % zero-crossing value
        elseif  and(signL==+1,signR==-1)  
            % if sign at the left = pos and sign at the right = neg:
            % zero-crossing, neg type (decelerating) 
            C           =   C+1                 ;   % counter + 1
            TypCross(C) =   -1                  ;   % zero-crossing type: -1
            LocCross(C) =   kk(i)               ;   % zero-crossing location
            ValCross(C) =   dd(i)               ;   % zero-crossing value
        end
    else
        % if current value not zero and current point is not the last point
        signL           =   sign(dd(i-1))       ;   % sign of value at the left
        if and(signL==-1,signC==+1)
            % if sign at the left = neg and current sign = pos:
            % zero-crossing, pos type (accelerating) 
            C           =   C+1                 ;   % counter + 1
            TypCross(C) =   +1                  ;   % zero-crossing type: +1
            LocCross(C) =   (kk(i-1)+kk(i))/2   ;   % zero-crossing location
            ValCross(C) =   (dd(i-1)+dd(i))/2   ;   % zero-crossing value
        elseif and(signL==1,signC==-1)
            % if sign at the left = pos and current sign = neg:
            % zero-crossing, neg type (decelerating) 
            C           =   C+1                 ;   % counter + 1
            TypCross(C) =   -1                  ;   % zero-crossing type: -1
            LocCross(C) =   (kk(i-1)+kk(i))/2   ;   % zero-crossing location
            ValCross(C) =   (dd(i-1)+dd(i))/2   ;   % zero-crossing value
        end
    end
end
Cross       =   [ LocCross  ValCross  TypCross ];   % bundle all info
Cross       =   Cross(1:C,:)                    ;   % remove unused space

% =========================================================================
function QR = IntegrateAnalysis(Extrem,Cross,N,dd)    

% convert info about extrema and zero-crossings into qualitative
% representation

nExtr       =       size(Extrem,1)              ;   % number of extrema
nCros       =       size(Cross,1)               ;   % number of zero crosssings
Pts         =   [   Extrem zeros(nExtr,1);...
                    Cross ones(nCros,1)     ]   ;   % join all essential points
[oo,ii]     =       sort(Pts(:,1))              ;   % sorted index based on location
Pts         =       Pts(ii,:)                   ;   % sort points

% add point for beginning first episode
Pts         =   [   1 zeros(1,3); Pts       ]   ;   % add a point at the beginning

if nCros==0 
    % A there are no zero crossings
    Y               =   dd./max(std(dd),eps)        ;   % scale with standard deivation
    if nExtr==0                                         
        % A1 no zero crossings, no extrema: one episode only
        Sign1       =   sign(sum(Y))                ;   % sign first derivative
        delta       =   Y(end)-Y(1)                 ;   % difference between end point and start point
        
%         Linear      =   (Y(1)+(0:N-1)'/(N-1)*delta) ;
%         figure
%         hold on
%         plot(Y,'b.')
%         plot(Linear,'r-')
        
%         Y           =   Y-Linear ;   % remove trend defined by first and last point
                
%         figure
%         hold on
%         plot(Y,'r.')

        Sign2       =   sign(delta)                 ;   % remaining info defines curvature

    else
        % A2 no zero crossings but extrema
        Sign1       =   sign(sum(Y))                ;   % sign first derivative
        Pts(:,3)    =   Pts(end,3)                  ;   % all points have same sign for first derivative
        Loc1        =   Pts(end-1,1)                ;   % start time second last point
        Loc2        =   Pts(end,1)                  ;   % start time last point
        Loc3        =   N                           ;   % end time
        if floor(Loc1)~=Loc1, Loc1 = floor(Loc1+[-.5 .5]) ; end   % if time index is no integer, make it an integer
        if floor(Loc2)~=Loc2, Loc2 = floor(Loc2+[-.5 .5]) ; end   % if time index is no integer, make it an integer
        if floor(Loc3)~=Loc3, Loc3 = floor(Loc3+[-.5 .5]) ; end   % if time index is no integer, make it an integer
        Y1          =   mean(Y(Loc1))               ;   % get corresponding value
        Y2          =   mean(Y(Loc2))               ;   % get corresponding value
        Y3          =   mean(Y(Loc3))               ;   % get corresponding value
        delta       =   2*Y2-Y1-Y3                  ;   % measure of curvature
        Sign2       =   sign(delta)                 ;   % sign of curvature
        nP          =   size(Pts,1)                 ;   % number of points
        dP          =   (1:nP)'-nP                  ;   % reverse index
        Sign1       =   ones(nP,1)*Sign1            ;   % expand sign first derivative
        Sign2       =   -Sign2*(-1).^dP             ;   % expand sign second derivative, power makes alternation
        
    end

else
    % B there are zero crossings
    
    % B1 sign first derivative
    Sign1                   =   Pts(:,3).*Pts(:,4)      ;   % sign of first derivative in each episode, zero for extrema
    Loc                     =   find(Sign1)             ;   % locate non-zero signs for first derivative (= zero-crossings)
    if ~isempty(Loc)    
        % not empty, there are zero-crossings
        nLoc                =   length(Loc)             ;   % number of zero crossings
        Sign1(1:Loc(1)-1)   =   -Sign1(Loc(1))          ;   % sign first derivative for episodes before first zero-crossing
        for iLoc=1:nLoc-1                                   % sign first derivative for episodes after first zero-crossing and before last zero-crossing
            vec             =   Loc(iLoc):Loc(iLoc+1)-1 ;   % locate the episodes between two zero crossings
            Sign1(vec)      =   Sign1(vec(1))           ;   % assign the proper sign for the first derivative
        end
        Sign1(Loc(nLoc)+1:end)  =   Sign1(Loc(nLoc))    ;   % sign first derivative for episodes after last zero-crossing
    else % empty
        if max(Extrem(:,2))>0,      SignX   =   1       ;   % if maximal extremum is positive type (max), then first derivative = pos
        elseif min(Extrem(:,2))<0,  SignX   =   -1      ;   % if minimal extremum is negative type (neg), then first derivative = neg
        else                        SignX   =   0       ;   % if all extrema are zero, then first derivative = 0
        end
        Sign1(:)            =   SignX                   ;   % assign derived sign to all
    end

    % B2 sign second derivative
    Sign2                   =   Pts(:,3).*(Pts(:,4)==0) ;   % sign of second derivative
    Loc                     =   find(Sign2)             ;   % non-zero -> extremum
    if ~isempty(Loc)                                        
        % not empty: there are extrema
        nLoc                =   length(Loc)             ;   % number of extrema
        Sign2(1:Loc(1)-1)   =   Sign2(Loc(1))           ;   % sign before first extrema
        for iLoc=1:nLoc-1                                   % sign second derivative for episodes after first extremum and before last extremum
            vec             =   Loc(iLoc):Loc(iLoc+1)-1 ;   % locate the episodes between two extrema
            Sign2(vec)      =   -Sign2(vec(1))          ;   % assign the proper sign for the second derivative
        end
        Sign2(Loc(nLoc):end)    =   -Sign2(Loc(nLoc))   ;   % sign second derivative for episodes after last extremum
    else
        % empty: no extrema -> all signs are the same
        if dd(end)>dd(1)
            Sign2(:)   =   1  ;
        elseif dd(end)<dd(1)
            Sign2(:)   =   -1  ;
        else
            Sign2(:)   =   0  ;
        end
            
    end
end

% bundle all information together, column 1: start index, 2: end index, 3:
% sign 1st derivative, 4: sign 2nd  derivative, 5: value of signal
QR      =   [ Pts(1:end,1)  [ Pts(2:end,1); N ] Sign1 Sign2 Pts(1:end,2) ] ;


% =========================================================================