function PlotQRT(QR,type,Ts)
%
%   PlotQRT
%   -----------------------------------------------------------------------
%
%   Vizualize Qualitative Representations of Trends. 
%
%   I/O: PlotQRT(QR,type,Ts)
%
%   inputs:
%       QR:         Cell array of qualitative representations. Each cell
%                   has a qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       type:       [optional] Define which primitives to use. 0: none, 1: monotonic,
%                   2: triangular
%                   [default=0] 
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 29/11/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<2
    type = 0 ;
end
if nargin<=2 || isempty(Ts)
    Ts = 1; 
end

% assert(any(type==3),'Unknown type of vizualization')

hold on
if ~isa(QR,'cell')
    QR    =   { QR }    ;
end
n   =   length(QR)    ;

XXX =   []  ;
YYY =   []  ;
CC  =   []  ;
for i=1:n
    Epimat          =   QR{i}                     ;
    if ~isempty(Epimat)

        
        nEpimat         =   size(Epimat,1)                  ;
        Epimat(:,5)     =   2*Epimat(:,3)+Epimat(:,4)       ;
        
        X1      =       Epimat(:,1)             ;   %   left X coordinates
        X2      =       Epimat(:,2)             ;   %   right X coordinates
        Y1      =       ones(nEpimat,1)*(i-1)+.5;   %   bottom Y coordinates
        Y2      =       Y1+1                    ;   %   top Y coordinates

        XX      =   [   X1  X1  X2  X2  ]'      ;   %   X coordinates
        YY      =   [   Y1  Y2  Y2  Y1  ]'      ;   %   Y coordinates
        C       =       Epimat(:,5)             ;   %   qualitative marker (-3 to +3)

        XXX     =   [   XXX  XX ]   ;
        YYY     =   [   YYY  YY ]   ;
        CC      =   [   CC; C ]     ;
       
    end
end

map = 1;

N       =   size(XXX,2)                 ;
CCC     =   reshape(CFD(CC,map),[1 N 3 ])   ;  % Actual Colors

XXX     =   (XXX-1)*Ts          	;
    
XMIN    =   min(XXX(:))         ;
XMAX    =   max(XXX(:))         ;
YMIN    =   min(YYY(:))         ;
YMAX    =   max(YYY(:))         ;
axis([XMIN XMAX  YMIN YMAX])    ;
% drawnow

patch(XXX,YYY,CCC,'LineStyle','none')
drawnow

LineWidth = 1.2 ;
PlotHorizontal((YMIN+1:1:YMAX-1),'w-','LineWidth',LineWidth)
set(gca,'Ydir','reverse');
if type % add characters
    select  =   find(XXX(3,:)-XXX(1,:)>=5)  ;
    CC      =   CC(select)                  ;
    XXX     =   XXX(:,select)               ;
    YYY     =   YYY(:,select)               ;
    
    QRstring    =   cellstr(Alphabet(CC,type)')  ;
    TextX       =   mean(XXX)               ;
    TextY       =   mean(YYY)               ;
%     nChar       =   length(QRstring)        ;
    text(TextX,TextY,QRstring,'Color','w','VerticalAlignment','middle','HorizontalAlignment','center','fontsize',20,'fontname','Helvetica-Narrow','fontweight','normal')
end

set(gca,'Ytick',1:n)
ylabel('Wavelet scale [-]')
drawnow
