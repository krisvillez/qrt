function [Y,c1,c2] = Mirror(Y,action,c1,c2)
%
%   Mirror
%   -----------------------------------------------------------------------
%
%   Padding of time series for qualitative analysis. Available types of
%   padding: 
%       0: zero-order       [ y(-k) =  y(1) ]
%       1: symmetric        [ y(-k) =  y(k) ]
%       2: anti-symmetric   [ y(-k) = -y(k) ]
%
%   I/O: [Y,c1,c2] = Mirror(Y,action,c1,c2)
%
%   inputs:
%       Y:          Data. Individual time series as columns.
%       action:     Indicate whether data is added (1) or cut off (2)
%       c1:         if action=1, type of padding before time series (0/1/2)
%                   if action=2, first element to keep
%       c2:         if action=1, type of padding after time series (0/1/2)
%                   if action=2, last element to keep
%
%   outputs:
%       Y:          Modified data.
%       c1:         if action=1, first index of original data
%                   if action=2, same as input c1
%       c2:         if action=1, last index of original data
%                   if action=2, same as input c2
%
%   Originally by:      Kris Villez, 13/09/2008
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

switch action
    case 1 % expand data
        nrow        =   size(Y,1)           ;   % number of rows
        
        Y1          =   GetData(Y,c1,1)     ;   % get left-hand side data according to mirror
        Y2          =   GetData(Y,c2,nrow)  ;   % get right-hand side data according to mirror

        Y           =   [ Y1(1:end-1,:) ; Y ; Y2(2:end,:) ]     ;   % bundle together
        c1          =   nrow                ;   % cut index to be used at left
        c2          =   nrow*2-1            ;   % cut index to be used at right
        
    case 2 % cut data
        Y           =   Y(c1:c2,:,:)        ;   % cut data at provided indices
        
end

% =========================================================================
function Y1 = GetData(Y,type,k)

nrow        =   size(Y,1)                   ;   % number of rows
if type == 0                                        % type 0 - zero-order padding: keep current value
    if k==1
        value = 0;
    else
        value = Y(k,:);
    end
    if k==1
        Y1          =   ones(nrow,1)*value     ;
    else
        Y1          =   ones(nrow,1)*value     ;
    end
else
    switch type
        case 1 % point-symmetric mirror
            Y1  =   flipud(Y)                   ;
            if k==1
                Y1  =   -Y1; %+2*ones(nrow,1)*Y(k,:)   ;
            else
                Y1  =   -Y1+2*ones(nrow,1)*Y(k,:)   ;
            end
        case 2 % symmetric mirror
            Y1  =   flipud(Y)                   ;
        otherwise
            error('unknown option');
    end
end
