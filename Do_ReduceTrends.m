function QRs   =   Do_ReduceTrends(QRs)
%
%   ReduceTrends
%   -----------------------------------------------------------------------
%
%   Reduce trends so that contiguous inflection points are removed
%
%   I/O: QR   =   Do_ReduceTrends(QR)
%
%   inputs:
%       QR:         Cell array of qualitative representations. One matrix
%                   in each cell, corresponding to one column in D. Each
%                   matrix has one row per identified triangular episode,
%                   each with 4 elements (start time, end time, sign 1st
%                   derivative, sign 2nd derivative).
%
%   outputs:
%       QR:         Cell array of qualitative representations. One matrix
%                   in each cell, corresponding to one column in D. Each
%                   matrix has one row per identified triangular episode,
%                   each with 4 elements (start time, end time, sign 1st
%                   derivative, sign 2nd derivative).
%
%   Originally by:      Kris Villez, 29/12/2010
%   Last update by:     Kris Villez, 29/12/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% ---------------------------------------------------
% settings for debugging
verbose     =   0   ;
visual      =   0   ;
% ---------------------------------------------------

P               =   length(QRs)          ;   % number of qualitative representations
for p=1:P       % for each scale, do reduction
    if verbose,  disp(['     ReduceTrends: Level - ' num2str(p) ]) , end
    QR          =   QRs{p}                  ;
    QR          =   OneInflectionOnly(QR)   ;
    QRs{p}      =   QR                      ;
end

% =========================================================================
function [QR] = OneInflectionOnly(QR)

% among contiguous inflection points, keep only the ones with maximum
% absolute value for the 1st derivative

warning off MATLAB:colon:operandsNotRealScalar

% locate extrema
LocEx               =       find(QR(2:end,3)~=QR(1:end-1,3))+1  ;

if isempty(LocEx)
    % A no extrema found
    nQR             =       size(QR,1)                      ;   % number of episodes
    if nQR>2                                                    % more than 2 episodes
        [oo,select] =       max(QR(2:end,5).*QR(2:end,3))   ;   % indicate location of inflection point with maximal absolute slope
        keep        =       select+1                        ;   % index of second episode to keep
        keep        =       unique([1 keep])                ;   % include first episode
        kend        =       QR(end,2)                       ;   % end time
        QR          =       QR(keep,:)                      ;   % remove unwanted episodes
        nQR         =       size(QR,1)                      ;   % update number of episodes
        QR(1:nQR,2) =   [   QR(2:nQR,1) ; kend  ]           ;   % make sure end time is consistent  
        QR(1,3:4)   =   [   QR(2,3)  -QR(2,4)   ]           ;   % make sure signs of derivatives are consistent
    end
else
    % B extrema are present
    
    % B1 first episode
    e               =       1                                   ;   % 
    SignEx          =       QR(LocEx(e),3)                      ;   % sign first derivative of first extremum
    Can             =       find(QR(1:LocEx(e)-1,4)==SignEx)    ;   % candidates: find episode before extremum with same sign for 1st drv
    [oo,select]     =       max(QR(Can,5).*QR(Can,3))           ;   % among those select the one with maximal absolute slope
    keep            =       Can(select)                         ;   % index of episodes to keep
    if keep~=1                                                      % if the first episode is not the one kept
        QR(1,[2 4]) =   [   QR(keep,1)      -QR(keep,4)     ]   ;   %   make sure end time and second derivative of new first episode are consistent
        QR(keep,2)  =       QR(LocEx(e),1)                      ;   %   set end time of kept episode to time of the extremum
    end
    keep            =       unique([1 keep])                    ;   % keep first and selected episode
    QR              =   [   QR(keep,:); QR(LocEx(e):end,:)  ]   ;   % remove unwanted episodes

    LocEx           =       find(QR(2:end,3)~=QR(1:end-1,3))+1  ;   % update location of extrema
    nLocEx          =       length(LocEx)                       ;   % update number of extrema
    m               =       size(QR,1)                          ;   % update number of episodes
    acc             =       1:LocEx(1)                          ;   % selected episodes

    for e=1:nLocEx                                                  % for all extrema
        SignEx              =       QR(LocEx(e),3)              ;   %   sign extremum
        if e==nLocEx                                                %   last extremum
            vec             =       LocEx(e)+1:m                ;   %       index of episodes
        else                                                        %   not the last episode
            vec             =       LocEx(e)+1:LocEx(e+1)-1     ;   %       index of episodes
        end
        Can                 =       find(QR(vec,4)==-SignEx)    ;   %       candidates to keep (matching sign)
        if ~isempty(Can) 
            % if candidate list is not empty
            [oo,select]     =       max(QR(Can,5).*QR(Can,3))   ;   % select of the one to keep
            keep            =       vec(Can(select))            ;   % get index of the second episode to keep
            QR(LocEx(e),2)  =       QR(keep,1)                  ;   % set first episode end time to the start time of the one to keep
            QR(keep,2)      =       QR(vec(end),2)              ;   % set second episode end time to the start time of the one to keep
            acc             =   [   acc LocEx(e) keep       ]   ;   % add the first and second episode to keep to list
        else
            acc             =   [   acc LocEx(e)            ]   ;   % add to list
        end
    end
    
    acc     =   unique(acc)     ;   % remove doubles
    QR      =   QR(acc,:)       ;   % update qualitative representation (remove unwanted episodes)

end