function PlotLinks(PTS,Links,Ts)
%
%   PlotLinks
%   -----------------------------------------------------------------------
%
%   Vizualize obtained links following alignments of essential points
%
%   I/O: PlotLinks(PTS,Links,Ts)
%
%   inputs:
%       PTS:        Cell array of point type sequences. Each cell contain
%                   the point type sequence for the corresponding
%                   qualitative representation in the input as a matrix.
%                   The first column is the time index of the point and the
%                   second column is the qualitative indicator for the
%                   point. See function QR2PTS.m for more details.
%       Links:      Cell array of links between aligned points in
%                   neighbouring scales. Each cell has the pairs of aligned
%                   points as a matrix with each row representing one
%                   alignment. The first column contains the indices of the 
%                   points in the first sequence. The second column
%                   contains the indicates in the indices of the aligned 
%                   points in the second sequence.
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 29/11/2009
%   Last update by:     Kris Villez, 30/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<3 || isempty(Ts)
    Ts = 1;
end
hold on
if ~isa(Links,'cell')
    Links     =   { Links }         ;
end
n           =   length(Links)     ;

XX = [];
YY = [] ;
for i=1:n
    PTSmat1     =   PTS{i}              ;
    PTSmat2     =   PTS{i+1}            ;
    
    Linksmat    =   Links{i}            ;
    if ~isempty(Linksmat)
        nLinksmat   =   size(Linksmat,1)                ;
        X           =   zeros(nLinksmat,2)              ;

        X(:,1)      =   PTSmat1(Linksmat(:,1))          ;   %   X coordinates
        X(:,2)      =   PTSmat2(Linksmat(:,2))          ;   %   X coordinates
        X           =   (X-1)*Ts    ;
        Y           =   ones(nLinksmat,1)*[i i+1]       ;   %   Y coordinates
        XX= [ XX; X];
        YY=[ YY; Y];
%         plot(X',Y','k-','LineWidth',2)
    end
end
        
plot(XX',YY','wo-','LineWidth',1)
        
drawnow
axis tight 
drawnow


