function Colour  =   CFD(Qualifier,Map)
%
%   CFD (Colours For Drawing) 
%   -----------------------------------------------------------------------
%
%   Obtain RGB colour codes corresponding to qualitative markers
%
%   I/O: Colour  =   CFD(index,Map)
%
%   inputs:
%       Qualifier:  [optional] vector of qualitative markers for episodes,
%                   as integers ranging from -3 to 3. If left unspecified,
%                   complete colour map is returned.
%       Map:        [optional] Colour map used. Use 1 for colour map
%                   (blue/red tones), use 2 for grey scale map.
%                   [default=1]    
%
%   outputs:
%       Colour:     Matrix of RGB colours for each element in Qualifier.
%                   Each of three columns represents one channel (RGB).
%
%   Originally by:      Kris Villez, 28/11/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<1 || isempty(Qualifier),  Qualifier   =   (-3:3)  ;   end % get complete map
if nargin<2 || isempty(Map),        Map         =   1       ;   end % unspecified map, use colour one

% verify whether qualifier are proper
%assert(all(Qualifier(:)>=-3),'At least one qualifier too low (mimimally: -3')
%assert(all(Qualifier(:)<=3),'At least one qualifier too high (maximally: +3')

% verify whether specified map is proper
nMap        =   2   ;
%assert(any((1:nMap)==Map),'Not a valid map Qualifier')


switch Map
    case 1
        % colour map
        Colour   =   [  0   0   1   ;
                        0   0   .8  ;
                        0   0   .3  ;
                        0   0   0   ;
                        1   0   0   ;
                        .8  0   0   ;
                        .3  0   0   ]   ;
    case 2
        % grey scale map
        Colour   =   [   5/7 2/7 1/7 4/7 7/7 6/7 3/7 ]'/2   ;
        Colour   =       mean(Colour,2)*ones(1,3)           ;
    otherwise
        error('unknown map')
end

Qualifier   =   Qualifier + 4       ;   % obtain actual indices
Colour      =   Colour(Qualifier,:) ;   % get corresponding RGB colours
