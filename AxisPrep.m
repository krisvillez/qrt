
%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% set(gcf,'Position',[200 200 800 600])
% set(lh,{'marker'},mrk,'MarkerSize',8,'MarkerEdgeColor','k');
set(gca,'FontSize',14)
set(gca,'FontWeight','normal')
set(gca,'LineWidth',1)
% set(gca,'Xtick',[0:8])
% set(gca,'Xlim',[-1 9])
% set(gca,'Ytick',[0:.25:1])
set(gca,'tickdir','out')
%horizontal_line([0:.25:1],'k:')
% horizontal_line([10:.25:11.7],'k:')
% set(gca,'Ylim',[9.9 11.7])
%set(gca,'Ylim',[0 1])
hold on
