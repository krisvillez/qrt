function QR = Do_Heuristics(WIT,method)
%
%   Do_Heuristics
%   -----------------------------------------------------------------------
%
%   Identify which episodes are to be included in the final qualitative
%   representation.
%
%   I/O: QR = Do_Heuristics(WIT,method)
%
%   inputs:
%       WIT:        Wavelet Interval Tree as obtained from Do_WIT.
%       method:     [optional] Indicate how triangular episodes should be
%                   selected. Four options available:
%                   'Original': as in original method
%                   'Witkin':   based on repeated application of Witkin's
%                               stability criterion
%                   'Top':      find representative episodes as highest
%                               scale available (coarse representation)
%                   'Bottom':   find representative episodes as lowest
%                               scale available (detailed representation)
%
%   outputs:
%       QR:         Final qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%
%   Originally by:      Kris Villez, 11/12/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

visual          =   0                       ;   % set visual to 1 for visuals of intermediate results (for debugging)
QR              =   []                      ;   % initialize

% 1. monotonic features
WIT1            =   WIT.Monotonic.QR        ;   % get Monotononic WIT
Rep             =   Heuristic(WIT1)         ;   % apply heuristic to interval tree to find relevant features
QR1             =   WIT1(Rep,:)             ;   % select relevant features from WIT
QR1             =   sortrows(QR1,2)         ;   % sort according to end time of features

% 2. triangular features
WIT             =   WIT.Triangular.QR       ;   % get Triangular WIT
Rep             =   Hierarchical(WIT,QR1,method)   ;    % apply heuristics to interval tree to find relevant features while accounting for monotonic representation
QR2             =   WIT(Rep,:)              ;   % select relevant features from WIT
QR2             =   sortrows(QR2,2)         ;   % sort according to end time of features

% 3. strip all unwanted info
QR1             =   QR1(:,1:4)              ;   % columns: start index / end index / sign first derivative / sign second derivative
QR2             =   QR2(:,1:4)              ;   % columns: start index / end index / sign first derivative / sign second derivative

% 4. save
QR.Monotonic    =   QR1                     ;
QR.Triangular   =   QR2                     ;


if visual
    figure
    subplot(2,1,1)
        PlotQRT(QR1)
    subplot(2,1,2)
        PlotQRT(QR2)
end

% =========================================================================
function Rep = Hierarchical(WIT,QR1,method) 

N                       =   size(WIT,1)                     ;   % number of features in Triangular WIT
nRep                    =   size(QR1,1)                     ;   % number of features in monotonic representation
Rep                     =   zeros(N,1)                      ;   % initialize: no features are representative
for iRep=1:nRep                                                 % run over all features in monotonic representation
    Loc                 =   find(WIT(:,13)==QR1(iRep,7))    ;   %   find those Triangular features that are covered by this monotonic feature
    SubWIT              =   WIT(Loc,:)                      ;   %   get sub-tree related to this monotonic feature
    switch upper(method)                                        %   depending on the method
        case {'WITKIN','WITKIN2'}                               %       if method based on repeated application of Witkin's stability criterion
            SubRep      =   Heuristic(SubWIT,method)               ;   %           apply Witkin's heuristic to subtree to select features
        case  'BOTTOM'                                          %       if method = 'bottom'
            nScale      =   max(SubWIT(:,6))                ;   %           maximal scale within subtree (most coarse scale, deepest in filter bank)
            %SubRep      =   find(SubWIT(:,6)==nScale)       ;   %           select features present at maximal scale
            SubRep      =   SubWIT(:,6)==nScale             ;   %           select features present at maximal scale
        case {'ORIGINAL','TOP'}                                 %       if method = �rginal' or 'top'  (most detailed scale, highest in filter bank)
            nScale      =   min(SubWIT(:,5))                ;   %           minimal scale within subtree
            %SubRep      =   find(SubWIT(:,5)==nScale)       ;   %           select features present at minimal scale
            SubRep      =   SubWIT(:,5)==nScale             ;   %           select features present at minimal scale
    end
    Rep(Loc(SubRep))    =   1                               ;   %   set selected features to representative
end
Rep                     =   find(Rep)                       ;   % convert binary sequence to numbers (index of features in WIT)

% =========================================================================
function Represent = Heuristic(WIT,method) 

if nargin<2 || isempty(method)
    method  = 'WITKIN';
end
% This function applies Witkin's stability criterion, starting from the
% coarsest scale in the provided WIT

N                               =   size(WIT,1)                         ;   % number of features
nScale                          =   max(WIT(:,6))                       ;   % maximum scale
Top                             =   find(WIT(:,6)==nScale)              ;   % find top features (those at the maximum scale, most coarse representation)
Span                            =   WIT(:,6)-WIT(:,5)+1                 ;   % the total span of scales

Active                          =   zeros(N,1)                          ;   % initialize: all feature inactive
Active(Top)                     =   1                                   ;   % initialize: activate features at top scale
Represent                       =   zeros(N,1)                          ;   % initialize: none of the features are in the final representation

while any(Active)   % while at least one feature is active
    Parent                      =   find(Active,1,'first')              ;   % find the first feature among those active
    Children                    =   find(WIT(:,8)==WIT(Parent,7) )      ;   % find the children (leaves)
    if isempty(Children)                                                    % if there are no children
        Active(Parent)          =   0                                   ;   %   set current feature to inactive
        Represent(Parent)       =   1                                   ;   %   include current feature in final representation
    else                                                                    % if there are children
        DeltaSpan               =   Span(Parent)-mean(Span(Children))   ;   %   compute the difference between the span of the parent feature and the average span of child features
        switch upper(method) 
            case 'WITKIN'
                Split   =   DeltaSpan<0   ;      %   if this difference is less than to zero, then accept split (conservative)
            case 'WITKIN2'
                Split   =   DeltaSpan<=0  ;     %   if this difference is less than or equal to zero, then accept split
        end
        if Split
            Active(Parent)      =   0                                   ;   %       deactivate current parent
            Active(Children)    =   1                                   ;   %       activate children
        else                                                                %   else, deny split
            Active(Parent)      =   0                                   ;   %       deactivate current parent
            Represent(Parent)   =   1                                   ;   %       include current feature in final representation
        end
    end
end
Represent                       =   find(Represent)                     ;   %   convert binary sequence into numbers

% =========================================================================
% =========================================================================