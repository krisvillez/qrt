function [Pointer,PathObj] = Do_Align_DynProgFW(type1,type2,time1,time2,envUpp,envLow,ncrit)

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% number of instants in each sequence
nk          =   size(time2,1)   ;
nk1         =   size(time1,1)   ;
vec1        =   1:nk1           ;

% initialize
k           =   1               ;

% find equal type elements within upper/lower limits
feasible    =   vec1(type1==type2(k))                                   ;   
feasible    =   feasible(and(feasible<=envUpp(k),feasible>=envLow(k)))  ;
    
StateObj    =   abs(time1(feasible)-time2(k))   ;   % distance for potential links

% initialize
PathObj     =   StateObj        ;   % path overall objective = first instant objective
PathEnd     =   feasible(:)     ;   % path end = current instants
p           =   size(PathEnd,1) ;   % no. of paths

% initialize pointer matrix
Pointer         =   zeros(4,nk1*nk) ;   % 0: pointing back to nothing
Pointer(1,1:p)  =   1               ;   % instant in seq 2 (1)
Pointer(2,1:p)  =   feasible(:)'    ;   % instants in seq 1

for k=2:nk
    
    % find equal type elements within upper/lower limits
    EqType      =   type1==type2(k)     ;
    feasible    =   vec1(EqType)        ;
    SatLim      =   and(feasible<=envUpp(k) , feasible>=envLow(k)) ;
    feasible    =   feasible(SatLim)    ;
    nfeas       =   length(feasible)    ;
    vecf        =   1:nfeas             ;

    if nfeas>=1
        StateObj    =   abs(time1(feasible)-time2(k))   ;   % distance for potential links
        
        if nfeas>ncrit     
            % if more paths are created than allowed, prune on the basis of
            % distance
            [StateObjSort]  =   sort(StateObj)              ;
            StateObjMax     =   StateObjSort(ncrit)         ;
            Select          =   StateObj<=StateObjMax       ;
            StateObj        =   StateObj(Select)            ;
            feasible        =   feasible(Select)            ;
            nfeas           =   length(feasible)            ;
        end

        % new path objective
        newPathObj  =   zeros(nfeas,1)                  ;

        for ifeas=1:nfeas % for each possible new state
            % find paths which can reach this state
            time1last       =   time1(PathEnd)                          ;
            feasiblePath    =   time1last<time1(feasible(ifeas))        ;            
            
            if any(feasiblePath)
                % compute new objective as obtained for each feasible path leading to
                % the proposed feasible state
                feasibleObj         =   PathObj(feasiblePath)+StateObj(ifeas)   ;
                
                % select path with minimum distance
                [minObj,minPath]    =   min(feasibleObj)                ;

                % store solution
                newPathObj(ifeas)   =   minObj  ;
                p                   =   p+1     ;
                Pointer(:,p)        = [ k   ;   feasible(ifeas) ;   ... 
                                        k-1 ;   PathEnd(minPath)    ];
                
            else
                % no paths available: dead end
                newPathObj(ifeas)   =   Inf     ;
                p                   =   p+1     ;
                Pointer(:,p)        = [ k   ;   feasible(ifeas) ;   ...
                                        0   ;   0                   ];
            end
        end
        if any(isinf(newPathObj))
            % remove solutions with infinite objective function\
            Inc         =   ~isinf(newPathObj)          ;
            feasible    =   feasible(Inc)               ;
            newPathObj  =   newPathObj(Inc)             ;
        end
    else
        % dead end: nothing links
        disp('Problem: dead end!')
    end
    
    % to next iteration:
    PathEnd =   feasible    ;
    PathObj =   newPathObj  ;

    %     figure
    %     hold on
    %     plot(time1-time2(k))
    %     plot(feasible,newPathObj,'o')
    %     plot(feasible,StateObj,'r+')

end

nPath       =   length(PathObj)         ;   % number of paths
if nPath>1 
    % if more than one, select best    
    [PathObj,Loc]   =   min(PathObj)    ;   % selected objective, choice
    p               =   p-nPath+Loc     ;   % last pointer corresponding to selected path    
end

Pointer     =   Pointer(:,1:p)          ;   % remove unnecessary pointers
