
%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

figure(han99)

row                 =   scale+1     ;
for col = 1:2
    if col == 1
        data    =   A1(k1:k2)   ;
        str1    =   'a'         ;
        shift   =   shiftL      ;
    elseif col==2
        data    =   D1          ;
        str1    =   'd'         ;
        shift   =   shiftR      ;
    end

    subhan = subplot(nrow,ncol,(row-1)*2+col)   ;

    AxisPrep
LineWidth =2;
    plot(T,data,'k-','LineWidth',LineWidth)
    axis tight
    if row~=nrow
        set(gca,'Xticklabel',[])
    else
        xlabel('Time [min.]')
    end
    set(gca,'Ytick',0)
    if col==1
        set(gca,'Ylim',[-1 1])
    else
        set(gca,'Ylim',[-.5 .5])
    end

    if ~Octave
        subroutine(subhan,shift)
    end

    if col==2
        set(gca,'Yticklabel',0)
        horizontal_line(0,'k-','LineWidth',1)
        if Octave
            ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle')
        else
            Xlim    =   get(gca,'Xlim')     ;
            Xrange  =   Xlim(2)-Xlim(1)     ;
            ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle','Position',[Xlim(2)+Xrange*15/200 0])
        end
        if row == 2
%             title(TitleString)
        end
    else
        set(gca,'Yticklabel',[])
        if Octave
            ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle')
        else
            Xlim    =   get(gca,'Xlim')     ;
            Xrange  =   Xlim(2)-Xlim(1)     ;
            ylabel([ str1 '_{' num2str(row-1) '}'],'rotation',0,'VerticalAlignment','middle','Position',[Xlim(1)-Xrange*15/200 0])
        end
    end
end