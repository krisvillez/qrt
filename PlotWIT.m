function PlotWIT(WIT,Monotonic,Mode,type,Ts)
%
%   PlotWIT
%   -----------------------------------------------------------------------
%
%   Vizualize Wavelet Interval Tree
%
%   I/O: PlotWIT(WIT,Monotonic,Mode,type,Ts)
%
%   inputs:
%       WIT:        Wavelet Interval Tree
%       Monotonic:  Decide to show Monotonic (0) or Triangular (1) WIT
%       Mode:       0: use time indices after alignment
%                   1: use time indices at original scale if available
%       type:       [optional] Define which primitives to use. 0: none, 1: monotonic,
%                   2: triangular
%                   [default=0] 
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs: N/A
%
%   Originally by:      Kris Villez, 11/12/2009
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


if nargin<2 || isempty(Monotonic)
    Monotonic   =   1   ;
end
if nargin<3 || isempty(Mode)
    Mode        =   0   ;
end
if nargin<4 || isempty(type)
    type        =   0   ;
end
if nargin<5 || isempty(Ts)
    Ts          =   1   ;
end
%assert(any((0:1)==Monotonic),'Not viable option')

if Monotonic,   WIT     =   WIT.Monotonic       ;
else            WIT     =   WIT.Triangular      ;
end


switch Mode
    case 0
        Type1   =   1                   ;
        Type2   =   1                   ;
    case 1
        if isfield(WIT,'Time'), 
            Times   =   WIT.Time            ;
            Type1   =   0                   ;
        else
            Type1   =   1                   ;
            Type2   =   1                   ;
        end
end

WIT         =   WIT.QR              ;
    
[N]         =   size(WIT,1)         ;
map =2;

if Type1
    X1      =   WIT(:,1)                    ;   %   left X coordinates
    X2      =   WIT(:,2)                    ;   %   right X coordinates
    if nargin < 3 || ( Type2 == 1 || isempty(Type) )
        X3  =   X2                          ;  
        X4  =   X1                          ;
    else
        X4  =   WIT(:,9)                    ;
        X3  =   WIT(:,10)                   ;
    end
    Y1      =   WIT(:,5)-1                  ;   %   bottom Y coordinates
    Y2      =   WIT(:,6)                    ;   %   top Y coordinates
    C       =   2*WIT(:,3)+WIT(:,4)         ;   %   Qualitative marker

    XX      =   [   X4  X1  X2  X3  ]'      ;   %   X coordinates
    YY      =   [   Y1  Y2  Y2  Y1  ]'+.5   ;   %   Y coordinates
    CC      =   reshape(CFD(C,map),[1 N 3 ] )   ;   % Actual Colors
    
else
    C2      =   size(Times,2)               ;
    C       =   C2/2                        ;
    Times1  =   Times(:,1:C)                ;
    Times2  =   Times(:,C+(1:C))            ;
    for i=1:N
        S1  =   WIT(i,5)                    ;
        S2  =   WIT(i,6)+1                  ;
        Y1  =   (S1:S2)                     ;
        Y2  =   fliplr(Y1)                  ;
        X1  =   Times1(i,Y1)                ;
        X2  =   Times2(i,Y2)                ;
        XX  =   [ X1 X2 ]'                  ;
        YY  =   [ Y1 Y2 ]'-1                ;
        C   =   2*WIT(i,3)+WIT(i,4) +4      ;   %   Color index
        CC  =   reshape(CFD(C,map),[1 1 3 ] )   ;   % Actual Colors
        
    end
end

XX      =   (XX-1)*Ts   ;

XMIN    =   min(XX(:))  ;
XMAX    =   max(XX(:))  ;
YMIN    =   min(YY(:))  ;
YMAX    =   max(YY(:))  ;
axis([XMIN XMAX  YMIN YMAX])

patch(XX,YY,CC,'LineStyle','none')
% set(gca,'Xlim',[XMIN XMAX ],'Ylim',[YMIN YMAX ]) ;

drawnow;
LineWidth = 1.2 ;
horizontal_line(YMIN+1:1:YMAX-1,'w-','LineWidth',LineWidth)
set(gca,'Ydir','reverse');
if type % add characters
    select      =   find(XX(3,:)-XX(1,:)>=5)    ;
    C           =   C(select)                   ;
    XX          =   XX(:,select)                ;
    YY          =   YY(:,select)                ;
    
    QRstring    =   cellstr(Alphabet(C,type)')  ;
    TextX       =   mean(XX)                    ;
    TextY       =   mean(YY)                    ;
    text(TextX,ceil(TextY),QRstring,'Color','w','VerticalAlignment','middle','HorizontalAlignment','center','fontsize',20,'fontname','Helvetica-Narrow','fontweight','normal')
end

xlabel('Time [min.]')
ylabel('Wavelet scale [-]')
set(gca,'Ytick',1:YMAX)