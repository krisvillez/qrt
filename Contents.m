% QRT
%
% Files
%   Alphabet             - Defines characters used for alphabetic representation
%   AxisPrep             - Visualisation - sets axis properties
%   CFD                  - Visualisation - sets colours for each primitive
%   demo                 - demonstration script
%   Do_Align             - Alignment of two representations based on dynamic program
%   Do_Align_DynProg     - Dynamic Program for alignment
%   Do_Align_DynProgFW   - Forward pass of Dynamic Program
%   Do_Filter_Trending   - Execute wavelet filtering and trending
%   Do_Filter_Trending_DisplayScript   - Visualisation - shows approximations and details
%                           this online version used as subroutine of Do_Filter_Trending
%                           use PlotWavelet otherwise
%   Do_Heuristics        - Apply heuristic rules for feature selection
%   Do_Linking           - Executes alignment for an array of representations
%   Do_ReduceTrends      - Removes consecutive inflection points
%   Do_Trending          - Find qualitative representation based on detail signal
%   Do_WIT               - Construct Wavelet Interval Tree (WIT)
%   FigPrep              - Visualization aid - sets figure properties
%   GetFilter            - Get proper filter coefficients
%   Mirror               - Add/Remove padded values for time series
%   PlotAlign            - Visualization - PlotAlign
%   PlotBoxes            - PlotBoxes
%   PlotHorizontal       - Visualisation - plots horizontal line
%   PlotLineage          - Visualisation - shows hierarchy in WIT
%   PlotLinks            - Visualisation - shows alignments 
%   PlotPTS              - Visualisation - plots essential points with type-related markers
%   PlotQRT              - Visualisation - Plot qualitative representation
%   PlotVertical         - Visualisation - plots vertical line
%   PlotWavelet          - Visualisation - plots wavelet approximations and details
%   PlotWIT              - Visualisation - shows Wavelet Interval Tree (WIT)
%   QR2PTS               - Convert qualitative representation to a sequence of essential points
%   QR2string            - Convert qualitative representation to an alphabetic string
%   QRT                  - Main function - Find Qualitative Representation of Trends 
%   VerifyOctave         - Check whether platform is Matlab or Octave (used for visualisation)
