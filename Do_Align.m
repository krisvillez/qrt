function Links = Do_Align(PTS1,PTS2)
%
%   Do_Align
%   -----------------------------------------------------------------------
%
%   Align two point type sequences (PTS)
%
%   I/O: Links  =   Do_Align(PTS1,PTS2)
%
%   inputs:
%       PTS1:       First time sequence as matrix with each row
%                   representing one point, the first column containing the time
%                   indices and the second column containing the
%                   qualitative indicators
%       PTS2:       Second time sequence as matrix with each row
%                   representing one point, the first column containing the time
%                   indices and the second column containing the
%                   qualitative indicators
%
%   outputs:
%       Links:      Pairs of aligned points with each row representing one
%                   alignment. The first column contains the indices of the
%                   points in the first sequence. The second column
%                   contains the indicates in the indices of the aligned
%                   points in the second sequence.
%
%   Originally by:      Kris Villez, 05/06/2011
%   Last update by:     Kris Villez, 05/06/2011
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% options for debugging
verbose =   0   ;
visual  =   0;

% get time instants
time1   =   PTS1(:,1)   ;
time2   =   PTS2(:,1)   ;
% get point types
type1   =   PTS1(:,2)   ;
type2   =   PTS2(:,2)   ;

% number of instants in sequences
nk              =   size(time2,1)    ;
nk1             =   size(time1,1)    ;

% ----------------------------------------
% NAIVE SOLUTION
% for each instant in seq. 2
vec1            =   1:nk1   ;
Links=  zeros(nk,2) ;
for k=1:nk
    
    feasible        =   vec1(type1==type2(k))           ;   % find instants in seq. 1 of equal type
    StateObj        =   abs(time1(feasible)-time2(k))   ;   % distance for each possible alignment
    
    [oo,minLoc]     =       min(StateObj)               ;   % find alignment with minimum warping distance
    Links(k,:)      =   [   feasible(minLoc) k    ]     ;   % store alignment
    
end
% ----------------------------------------
% DYNAMIC PROGRAMMING

% difference in aligned instants
Steps                   =   diff(Links(:,1))    ;   

if all(Steps>0) 
    % if all positive -> naive solution = ok
    if verbose, disp('      Naive solution works'), end
    
else    
    % some equal or negative -> dynamic programming necessary
    if verbose, disp('      Naive solution does not work'), end
    
    % find upper/lower limits (envelopes) for instants in seq. 1
    % corresponding to instants in seq. 2
    [EnvUpp,EnvLow] =   Envelopes(Links,type1,type2);
    
%     if visual
%         figure
%         hold on
% %         plot(time2(Links(:,2)),time1(Links(:,1)),'o','MarkerFaceColor','b')
%         plot(time2(Links(:,2)),time1(EnvUpp),'r.-','MarkerFaceColor','r')
%         plot(time2(Links(:,2)),time1(EnvLow),'g.-','MarkerFaceColor','g')
%     end
    
    % Difference between upper and lower limit. If EnvGap =0, then upper
    % and lower limit are the same, meaning that solution is known for
    % these instants in seq. 2
    EnvGap =   abs(EnvUpp-EnvLow)   ;
    
%     visual =1;
%     if visual
%         figure, 
%         plot(EnvGap>0) ;
%     end

    kprev           =   1                                           ;   % previous instant in seq. 2
    k               =   kprev-1+find(EnvGap(kprev:end)>0,1,'first') ;   % current instant in seq. 2
    sec2            =   kprev:k-1                   ;   % set of contiguous instants for which gap=0
    Links           =   zeros(nk,2)                 ;
    Links(sec2,:)   = [ EnvUpp(sec2)    ;   sec2 ]' ;   % store solution
    
    % iter            =   0   ;   
    while k<nk
        
        % iter=iter+1 ;
    
        kprev       =   k                                           ;   % previous instant in seq. 2
        k           =   kprev-1+find(EnvGap(kprev:end)==0,1,'first');   % current instant in seq. 2
        if isempty(k)
            k       =   nk+1      ;   % last instant in seq. 2
        end
        
        sec2        =   kprev:k-1                       ;   % sec2: set of contiguous instants for which gap~=0
        time2sec    =   time2(sec2)                     ;   % time vector  for sec2
        type2sec    =   type2(sec2)                     ;   % type vector  for sec2
        sec1        =   EnvLow(kprev):EnvUpp(k-1)       ;   % sec1: set of candidate instants in seq. 1
        time1sec    =   time1(sec1)                     ;   % time vector  for sec2
        type1sec    =   type1(sec1)                     ;   % type vector  for sec2
        EnvUppsec   =   EnvUpp(sec2)-EnvLow(kprev)+1    ;   % upper limits for this section
        EnvLowsec   =   EnvLow(sec2)-EnvLow(kprev)+1    ;   % lower limits for this section

        % execute dynamic programming for section 
        Linkssec    =   Do_Align_DynProg(time1sec,time2sec,type1sec,type2sec,EnvUppsec,EnvLowsec)    ;
        
        % store found links
        Links(sec2,:)   =   [Linkssec(:,1)+EnvLow(kprev)-1  sec2(:) ]    ;
        
        if visual
            figure
            hold on
            plot((Links(sec2,2)),(Links(sec2,1)),'o','MarkerFaceColor','b')
            plot((Links(sec2,2)),(EnvUpp(sec2)),'r.-','MarkerFaceColor','r')
            plot((Links(sec2,2)),(EnvLow(sec2)),'g.-','MarkerFaceColor','g')
            
%             figure
%             hold on
%             plot(time2sec(Linkssec(:,2)),time1sec(Linkssec(:,1)),'o','MarkerFaceColor','b')
%             plot(time2sec(Linkssec(:,2)),time1sec(EnvUppsec),'r.-','MarkerFaceColor','r')
%             plot(time2sec(Linkssec(:,2)),time1sec(EnvLowsec),'g.-','MarkerFaceColor','g')
        end

        kprev               =   k                                       ;   % previous instant in seq. 2
        k                   =   kprev-1+find(EnvGap(kprev:end)>0,1,'first') ;   % current instant in seq. 2
        if isempty(k)
            k   =   nk+1  ;
        end
        sec2            =   kprev:k-1                       ;   % set of contiguous instants with 0 gap
        Links(sec2,:)   = [ EnvUpp(sec2)    ;   sec2    ]'  ;   % store links
        
    end
end

% find and retain true links (not zero)
inc     =   Links(:,1)~=0   ;
Links   =   Links(inc,:)    ;


% if visual
%     figure
%     hold on
%     plot(time2(Links(:,2)),time1(Links(:,1)),'o','MarkerFaceColor','b')
%     plot(time2(Links(:,2)),time1(EnvUpp),'r.-','MarkerFaceColor','r')
%     plot(time2(Links(:,2)),time1(EnvLow),'g.-','MarkerFaceColor','g')
% 
%     %             figure
%     %             hold on
%     %             plot(time2sec(Linkssec(:,2)),time1sec(Linkssec(:,1)),'o','MarkerFaceColor','b')
%     %             plot(time2sec(Linkssec(:,2)),time1sec(EnvUppsec),'r.-','MarkerFaceColor','r')
%     %             plot(time2sec(Linkssec(:,2)),time1sec(EnvLowsec),'g.-','MarkerFaceColor','g')
% end
% 
% Steps                   =   diff(Links(:,1))                    ;
% if visual
%     figure
%     plot(Steps)
% end

% -------------------------------------------------------------------------

function [EnvUpp,EnvLow]    =   Envelopes(Links,type1,type2)

nk          =   length(type2)   ;   % number of instants in seq 2
nk1         =   length(type1)   ;   % number of instants in seq 1

% -------------------------
% Upper limits
EnvUpp      =   zeros(1,nk) ;   % pre-allocation
k           =   1           ;   % first instant
EnvUpp(k)   =   Links(k,1)  ;   % actual link = upper limit
vec1        =   1:nk1       ;   

for k=2:nk  % for all subsequent instants in seq 2
    if Links(k,1)>EnvUpp(k-1) 
        % if established link follows chronology, keep it as upper limit
        EnvUpp(k)   =   Links(k,1)  ;
    else
        % if not:
        feasible    =   vec1(type1==type2(k))               ;   % find equal type elements
        loc         =   find(feasible>EnvUpp(k-1),1,'first');   % find the first one which follows chronological order
        if isempty(loc) 
            % if empty, take last instant in seq. 1 as upper limit, also for all subsequent instants in seq 2
            EnvUpp(k:end)   =   nk1             ;
            break
        else
            % not empty: store
            EnvUpp(k)   =   feasible(loc) ;
        end
    end
end

% -------------------------
% Lower limits
EnvLow      =   zeros(1,nk) ;   % pre-allocation
k           =   nk          ;   % last instant
EnvLow(k)   =   Links(k,1)  ;   % actual link = lower limit

for k=nk-1:-1:1 % for all preceding instants in seq 2
    if Links(k,1)<EnvLow(k+1)
        % if established link follows chronology, keep it as lower limit
        EnvLow(k)   =   Links(k,1)  ;
    else
        % if not:
        feasible    =   vec1(type1==type2(k))               ;   % find equal type elements
        loc         =   find(feasible<EnvLow(k+1),1,'last') ;   % find the last one which follows chronological order
        if isempty(loc)
            % if empty, take first instant in seq. 1 as lower limit, also for all preceding instants in seq 2            
            EnvLow(1:k)   =   1             ;
            break
        else
            EnvLow(k)   =   feasible(loc) ;
        end

    end
end

% visual  =   0   ;
% if visual
%     figure
%     hold on
%     plot(Links(:,2),Links(:,1),'o','MarkerFaceColor','b')    
%     plot(Links(:,2),EnvUpp,'r.-','MarkerFaceColor','r')
%     plot(Links(:,2),EnvLow,'g.-','MarkerFaceColor','g')
% end