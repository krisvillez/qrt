function FigHan = PlotAlign(QR,Links,Ts)
%
%   PlotAlign
%   -----------------------------------------------------------------------
%
%   Vizualize alignments
%
%   I/O: Handle = PlotAlign(QR,PTS,Links,Ts)
%
%   inputs:
%       QR:         Cell array of qualitative representations. Each cell
%                   has a qualitative representation as a matrix with each
%                   row representing an episode. Columns 1 to 4 contain the
%                   start time, the end time, the sign of the first
%                   derivative and the sign of the second derivative for
%                   each episode.
%       PTS:        Cell array of point type sequences. Each cell contain
%                   the point type sequence for the corresponding
%                   qualitative representation in the input as a matrix.
%                   The first column is the time index of the point and the
%                   second column is the qualitative indicator for the
%                   point. See function QR2PTS.m for more details.
%       Links:      Cell array of links between aligned points in
%                   neighbouring scales. Each cell has the pairs of aligned
%                   points as a matrix with each row representing one
%                   alignment. The first column contains the indices of the 
%                   points in the first sequence. The second column
%                   contains the indicates in the indices of the aligned 
%                   points in the second sequence.
%       Ts:         [optional] sample time
%                   [default=1]     
%
%   outputs:
%       Handle:     Figure handle
%
%   Originally by:      Kris Villez, 10/10/2010
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<3 || isempty(Ts)
    Ts = 1;
end

PTS =   QR2PTS(QR)  ;

FigHan = figure ;
FigPrep
AxisPrep
PlotQRT(QR,2,Ts)
PlotLinks(PTS,Links,Ts)
title('Alignment of qualitative representations')
AxisPrep
xlabel('Time [min.]')
ylabel('Wavelet scale [-]')
drawnow;
axis tight
drawnow;



