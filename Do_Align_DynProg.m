function Links = Do_Align_DynProg(time1,time2,type1,type2,envUpp,envLow)

%   Do_Align_DynProg
%   -----------------------------------------------------------------------
%
%   This is a dynamic programming algorithm which finds an alignments of
%   points of the same type in two sequences
%
%   I/O: Links = Do_Align_DynProg(time1,time2,type1,type2,envUpp,envLow)
%
%   inputs:
%       Time1:      Vector with times in first sequence     [Px1]
%       Time2:      Vector with times in second sequence    [Qx1]
%       Type1:      Vector with types in first sequence     [Px1]
%       Type2:      Vector with types in second sequence    [Qx1]
%       EnvUpp:     Vector with lower limit for instant number in seq 1 for
%                   each instant in seq 2 [Qx1]
%       EnvLow:     Vector with upper limit for instant number in seq 1 for
%                   each instant in seq 2 [Qx1]
%
%   outputs: 
%       Links:      Links between aligned points in the two sequences as a
%                   matrix with each row representing one alignment. The
%                   first column contains the indices of the points in the
%                   first sequence. The second column contains the
%                   indicates in the indices of the aligned points in the
%                   second sequence.
%
%   Originally by:      Kris Villez, 31/10/2010
%   Last update by:     Kris Villez, 06/04/2011
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% for debugging:
verbose =   0   ;

% number of instants in each sequence:
nk      =   size(time2,1)   ;
nk1     =   size(time1,1)   ;

nskip   =   0               ;
% nskip = maximal number of instants in seq 2 that can be skipped.
% Initiallly at zero. However, if no solution is found it can be increased
% until a solution is found.

ncrit   =   5               ;   
% ncrit = maximal number of paths that is entertained at any point
% - Setting ncrit lower speeds up the computation
% - In some cases this can lead to not finding a solution. For this reason,
% ncrit is increased at the end of the while loop if no solution is found

if nargin<5 || isempty(envUpp)
    envUpp = inf(nk,1);
end

if nargin<6 || isempty(envLow)
    envLow = -inf(nk,1);
end
% -------------------------------------------------------------------------
% FORWARD PASS

SolutionFound   =   0   ;   
% initialize, becomes 1 (true) if a solution has been found

while ~SolutionFound
    
    %[ nskip ncrit ]
    
    if nskip==0
        
        % execute forward pass of dynamic program
        [Pointer,PathObj] =   Do_Align_DynProgFW(type1,type2,time1,time2,envUpp,envLow,ncrit);
    
    else
        % if some instants can be skipped,
%         assert(nskip<=2,'This situation never occurred before. Check solution')
        % initialize
        arr         =   0                   ;
        PathObjArr  =   Inf*ones(1,nskip+1) ;
        PointerArr  =   cell(1,nskip+1)     ;
        IncMem      =   cell(1,nskip+1)     ;

        for iskip=1:nskip+1
            % this code evaluates all possibilities for skipping:
            % e.g., if 2 instants are skipped, then:
            %   first:  both at right
            %   second: one left, one right
            %   third:  both at left
            
            % ----------------------------------------------------
            % skip following locations in sequence 2
            skip        =   [ 1:(iskip-1)   nk-nskip+(iskip:nskip) ];
            inc         =   setdiff(1:nk,skip)  ;   % kept instances in seq 2
            type2skip   =   type2(inc)          ;   % selection
            time2skip   =   time2(inc)          ;   % selection
            envUppskip  =   envUpp(inc)         ;   % selection
            envLowskip  =   envLow(inc)         ;   % selection

            % execute forward pass of dynamic program
            [Pointer,PathObj] =   Do_Align_DynProgFW(type1,type2skip,time1,time2skip,envUppskip,envLowskip,ncrit);

            % if solution available, store
            if ~isempty(PathObj)
                arr             =   arr+1       ;
                PathObjArr(arr) =   PathObj     ;
                PointerArr{arr} =   Pointer     ;
                IncMem{arr}     =   inc         ;
            end
        end
        
        % ----------------------------------------------------
        % internal points:
        %   philosophy is that internal spuriously generated essential
        %   points exist in neighbouring pairs in the trending result.
        %   Here, each of the possible pairs is evaluated for removal. This
        %   is only done if no other solution was found yet.
        %  
        if and(arr==0,nskip==2)
            % locate pairs
            AbsType2    =   abs(type2)'                             ;
            Pair       =   find(AbsType2(2:end)==AbsType2(1:end-1)) ;
            
%             figure
%             hold on
%             stem(AbsType2,'.-')
%             stem(Pair,AbsType2(Pair),'ro')
            
            nPair       = length(Pair)    ;
            if nPair>=1
                for iPair=1:nPair
                    
                    skip        =   Pair(iPair)+[0 1] ;
                    
                    inc         =   setdiff(1:nk,skip)  ;   % kept instances in seq 2
                    type2skip   =   type2(inc)          ;   % selection
                    time2skip   =   time2(inc)          ;   % selection
                    envUppskip  =   envUpp(inc)         ;   % selection
                    envLowskip  =   envLow(inc)         ;   % selection
                    
                    % execute forward pass of dynamic program
                    [Pointer,PathObj] =   Do_Align_DynProgFW(type1,type2skip,time1,time2skip,envUppskip,envLowskip,ncrit);
                    
                    % if solution available, store
                    if ~isempty(PathObj)
                        arr             =   arr+1       ;
                        PathObjArr(arr) =   PathObj     ;
                        PointerArr{arr} =   Pointer     ;
                        IncMem{arr}     =   inc         ;
                    end
                end
            end
        end
        % ----------------------------------------------------
        % if at least 1 solution available, find best
        if arr>0
            PathObjArr      =   PathObjArr(1:arr)       ;
            [PathObj,Loc]   =   min(PathObjArr)         ;
            Pointer         =   PointerArr{Loc}         ;
            Inc             =   IncMem{Loc}             ;
            Pointer(1,:)    =   Inc(Pointer(1,:))       ;
            sel             =   Pointer(3,:)~=0         ;
            Pointer(3,sel)  =   Inc(Pointer(3,sel))     ;
        end
        
        
    end
    
    if isempty(PathObj)
        % if no solution found
        if ncrit >nk1   % if number of paths too large
            % allow to skip instants in seq 2
            nskip           =   nskip+1 ;
            
            if verbose, disp(['                     No solution - Allowing to skip ' num2str(nskip) ' features.']), end
            
        else
            % allow for more paths to be entertained simultaneously
            ncrit           =   ncrit *2 ; 
        end
    else
        SolutionFound   =   1       ;
    end
end



        
% % -------------------------------------------------------------------------
% % SOLUTION CHECK
% 
% % check whether pointers are unique
% PP          =   Pointer(:,1:2)'    ;
% assert(size(PP,1)==size(unique(PP,'rows'),1),'Multiple appearance of same pointer')
% clear PP

% -------------------------------------------------------------------------
% BACKWARD PASS

% backward passes are used to find actual path

nk      =   size(time2,1)   ;   % number of instants in seq 2
Links   =   zeros(nk,2)     ;   % pre-allocate
Contin  =   1               ;   % initialize: while loop on
p       =   size(Pointer,2) ;   % initialize: last pointer

while Contin 

    Pointer     =   Pointer(:,1:p)  ;   % remove unneccesary pointers
    np          =   size(Pointer,2) ;   % number of pointers

    % register current:
    k           =   Pointer(1,p)    ;   % instant in sequence 2
    Links(k,:)  =   Pointer(1:2,p)  ;   % store instant in sequence 2 and 1
    
    % new pointer index
    p           =   find(all(Pointer(1:2,:)==Pointer(3:4,p)*ones(1,np)),1,'last')   ;
    Contin      =  ~isempty(p)      ;   % empty -> no further pointers

end

% -------------------------------------------------------------------------
% re-arrange final result
Links   =   [   Links(:,2)  Links(:,1)  ];

return