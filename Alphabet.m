function String = Alphabet(Qualifier,Type)
%
%   Alphabet
%   -----------------------------------------------------------------------
%
%   Obtain characters according to qualitative marker (-3 to 3). The vector
%   of integers is converted into a string.
%
%   I/O: String = Alphabet(qualifier,type)
%
%   inputs:
%       Qualifier:  [optional] vector of qualitative markers for episodes,
%                   as integers ranging from -3 to 3. If left unspecified,
%                   complete alphabet is returned.
%       Type:       [optional] specify whether monotonic or triangular
%                   primitives are used. Use 1 for monotonic, 2 for
%                   triangular. [default=2]    
%
%   outputs:
%       String:     String consisting of characters corresponding to
%                   qualitative markers
%
%   Originally by:      Kris Villez, 03/10/2010
%   Last update by:     Kris Villez, 31/10/2010
%
%   -----------------------------------------------------------------------
%

%

% -------------------------------------------------------------------------
% Copyright 2006-2012 Kris Villez
%
% This file is part of the QRT Toolbox for Matlab/Octave. 
% 
% The QRT Toolbox is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% The QRT Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the QRT Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<1 || isempty(Qualifier),  Qualifier   =   (-3:3)  ;   end % get complete map
if nargin<2 || isempty(Type),       Type        =   2       ;   end % unspecified type, then use triangular primitives

% verify whether qualifiers are proper
%assert(all(Qualifier(:)>=-3),'At least one qualifier too low (mimimally: -3')
%assert(all(Qualifier(:)<=3),'At least one qualifier too high (maximally: +3')

% verify whether specified type is proper
nType       =   2   ;
%assert(any((1:nType)==Type),'Not a valid set of characters')

if Type == 1,   Characters  =   'LLLFUUU'   ;   % Signs first derivative: L:- / F:0 / U:+
else            Characters  =   'BECFAGD'   ;   % Signs derivatives: B:-- / E:-0 / C:-+ / F:00 / A:+- / G:+0 / D:++ 
end

Qualifier   =   Qualifier + 4               ;   % obtain actual indices
String      =   Characters(Qualifier)       ;   % get corresponding characters
